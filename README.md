# TORASU java
[![language: JAVA](https://img.shields.io/badge/language-JAVA-b07219)](https://en.wikipedia.org/wiki/Java_%28programming_language%29)
[![java: OpenJDK8](https://img.shields.io/badge/java-OpenJDK8-e87919)](https://openjdk.java.net/projects/jdk8/)
[![build: Gradle](https://img.shields.io/badge/build-Gradle-0dc5b9)](https://gradle.org/)
[![license: LGPLv3](https://img.shields.io/badge/license-LGPLv3-blue)](https://choosealicense.com/licenses/lgpl-3.0/)
[![pipeline status](https://gitlab.com/hctulip/torasu/core/torasu-java/badges/master/pipeline.svg)](https://gitlab.com/hctulip/torasu/core/torasu-java/pipelines) 
[![download latest slim build](https://img.shields.io/badge/download-Latest%20Slim-yellowgreen)](https://gitlab.com/hctulip/torasu/core/torasu-java/-/jobs/artifacts/master/download?job=torasu-java)
[![download latest full build](https://img.shields.io/badge/download-Latest%20Full-yellowgreen)](https://gitlab.com/hctulip/torasu/core/torasu-java/-/jobs/artifacts/master/download?job=torasu-java-std)

Java Implementation of the TORASU Compute Framework

For documentation on TORASU in general see [TORASU-docs](https://gitlab.com/hctulip/torasu/torasu-docs "TORASU Compute Framework Documentation") 
