package de.hcinc.torasu.core;

import java.sql.ResultSet;
import java.util.Map;

import org.json.JSONObject;

import de.hcinc.torasu.core.RenderContext.RenderModifier;
import de.hcinc.torasu.core.Renderable.RenderResult.ResultSegment;
import de.hcinc.torasu.log.LogInstruction;
import de.hcinc.torasu.pipeline.ExecutionInterface;
import de.hcinc.torasu.pipeline.SystemResourceInterface;

/**
 * <ul>
 *	<li>The Renderable is an Element, which can generate a result</li>
 *	<li>Which result should be generated is defined in the ResultSettings</li>
 *	<li>Renderables can receive mandatory modifications via the RenderContext</li>
 *	<li>Renderables can promise integrated calculations called RenderModifier</li>
 *	<li>Since Renderables are derived from Elements, 
 *		so they have the same Ready/Dependence/Metadata/etc features</li>
 * </ul>
 */
public interface Renderable extends Element {

	/**(Only when Ready) 
	 * Renders the Renderable to the information in the RenderWrangler.
	 * 
	 * @param rw The {@link RenderWrangler} that is containing the 
	 * {@link RenderContext} and {@link ResultSettings}, as well as 
	 * the {@link SystemResourceInterface} and {@link ExecutionInterface}
	 * 
	 * @return The Result of the rneder as a {@link RenderResult}
	 */
	public RenderResult render(RenderWrangler rw);
	
	/**(When Ready: More Info / Information change) 
	 * Gives the requested data about the Renderable 
	 * (<b>NOTE:</b> Make sure to ready the fitting operations to the keys)
	 * 
	 * @param keys The keys of the of the requested content
	 * @param rctx The {@link RenderContext} the {@link RenderableData} should be fetched at
	 * 
	 * @return The requested {@link RenderableData}
	 */
	public RenderableData data(String[] keys, RenderContext rctx);
	
	/**(Only when Ready) Idenificators of the types of RenderModifier that can be applied over the {@link RenderContext} 
	 * <br>
	 * - Those promises may not change after other RenderModifier have been added to the {@link RenderContext}, 
	 * all promised RenderModifier(s) have to be able to be applied at the same time. 
	 * (<b>NOTE:</b> Make sure to ready the fitting operations to the requests)
	 * 
	 * @param requests The identificators of the requested modifications that are requested to get promised
	 * @param rctx The current {@link RenderContext} the promises should happen on
	 * 
	 * @return Idenificators of the available types of {@link RenderModifier}
	 */
	public String[] promiseRenderModification(String[] requests, RenderContext rctx);
	
	/**Get the {@link ExecutionRequirements} for the renderable
	 *  
	 * @param rctx The {@link RenderContext} the {@link ExecutionRequirements} should be get on
	 * @param segments The segments that will be rendered
	 * 
	 * @return The {@link ExecutionRequirements}
	 */
	public ExecutionRequirements getExecutionRequirements(RenderContext rctx, String[] segments);
	
	/**Gets {@link PipelineSettings} for this {@link Renderable}, which contains information on algorithm picking and attributes recognized by PipelineModules
	 */
	public PipelineSettings getPipelineSettings();
	
	
	/**The {@link RenderWrangler} is a collection of data required to run a render, 
	 * containing the {@link RenderContext} and {@link ResultSettings} as usable data, 
	 * but also containing an {@link SystemResourceInterface} for system-resource management 
	 * and a {@link ExecutionInterface} to manage child renders/target-executions
	 */
	public static class RenderWrangler {
		
		private RenderContext rctx;
		private ResultSettings rs;
		private SystemResourceInterface sysri;
		private ExecutionInterface ei;
		private String[] ealg;
		private LogInstruction logi;

		/**Create a {@link RenderWrangler}
		 * @param rctx The {@link RenderContext} of the render
		 * @param rs The {@link ResultSettings} of the render
		 * @param sysri The interface for system-resource management ({@link SystemResourceInterface}) of the render
		 * @param ei The {@link ExecutionInterface} to render child-renderables or execute child-targets
		 * @param ealg The identifiers of the available ElementAlgorithms
		 * 
		 * @see RenderContext
		 * @see ResultSettings
		 * @see SystemResourceInterface
		 * @see ExecutionInterface
		 * TODO @see AlgorithmMap
		 * 
		 */
		public RenderWrangler(RenderContext rctx, ResultSettings rs, SystemResourceInterface sysri, ExecutionInterface ei, String[] ealg, LogInstruction logi) {
			this.rctx = rctx;
			this.rs = rs;
			this.sysri = sysri;
			this.ei = ei;
			this.ealg = ealg;
			this.logi = logi;
		}
		
		/**Gets the {@link RenderContext} of the render
		 * 
		 * @see RenderContext
		 */
		public RenderContext getRenderContext() {
			return rctx;
		}
		
		/**Gets the {@link ResultSettings} of the render
		 * 
		 * @see ResultSettings
		 */
		public ResultSettings getResultSettings() {
			return rs;
		}
		
		/**Gets the interface for system-resource management ({@link SystemResourceInterface}) of the render
		 * 
		 * @see ResultSettings
		 */
		public SystemResourceInterface getSystemResourceInterface() {
			return sysri;
		}
		
		/**Gets the {@link ExecutionInterface} to render child-renderables or execute child-targets
		 * 
		 * @see ExecutionInterface
		 */
		public ExecutionInterface getExecutionInterface() {
			return ei;
		}
		
		/**Get the identifiers of the available ElementAlgorithms
		 * 
		 * 
		 */
		public String[] getEnabledAlgorithms() {
			return ealg;
		}
		
		/**
		 * @return The log-instruction for the render
		 */
		public LogInstruction getLogInstruction() {
			return logi;
		}
		
	}
	
	
	/**The {@link RenderResult} delivers the result of a render, errors and other data about the result.
	 *
	 */
	public static class RenderResult {
		
		private int status;
		private ResultSegment[] result;
		private String[] appModifiers;

		/**Instantiate a {@link RenderResult}-object
		 * @param status The status of the render 0=OK, -5=Internal Error, -1=Unknown error   (Generally: <0 = Error: Result (partially) Invalid) 
		 * @param result The Result segments defined in the {@link ResultSettings}
		 * @param appModifiers The modifiers that have been applied during the render process
		 * 
		 */
		public RenderResult(int status, ResultSegment[] result, String...appModifiers) {
			this.status = status;
			this.result = result;
			this.appModifiers = appModifiers;
		}
		
		/**
		 * @return The status of the render 0=OK, -5=Internal Error, -1=Unknown error   (Generally: <0 = Error: Result (partially) Invalid)
		 */
		public int getStatus() {
			return status;
		}
		
		/**
		 * @return The Result segments defined in the {@link ResultSettings}
		 */
		public ResultSegment[] getResult() {
			return result;
		}
		
		/**Get the applied {@link RenderModifier}(s), to validate that all modifiers that were promised and put got applied.
		 * @return The applied {@link RenderModifier}(s) that have been applied during the render.
		 */
		public String[] getAppModifiers() {
			return appModifiers;
			
		}
		
		public static String getResultDisplay(RenderResult res) {
			
			int status = res.getStatus();
			
			StringBuilder display = new StringBuilder();
			
			if (status >= 0) {
				ResultSegment[] resArr = res.getResult();
				display.append("RenderResult[" + status + "] {" + resArr.length + "}\n");
				for (int i = 0; i < resArr.length; i++) {
					display.append("\t" + resArr[i].toString());
				}
			} else {
				display.append("RenderResult[" + status + "]\n");
			}
			
			return display.toString();
		}
		
		/**A ResultSegment is the result of the render-process, which was configured by the ResultSegmentSettings in the {@link ResultSettings}
		 * <ul>
		 * 	<li>The key that result-segment got mapped to in the ResultSettings/ResultSegmentSettings</li>
		 * 	<li>Contains the identifier of the result-format</li>
		 * 	<li>The actual result</li>
		 * 	<li><i>Note: Information like formats/sub-formats and format-specific data are not stored here 
		 * - they have to be available through methods in the result object if they need to get accessed afterwards</li>
		 * </ul>
		 */
		public static class ResultSegment {
			
			private String key;
			private String formatIdent;
			private Object result;

			/**A part of the {@link RenderResult} which come in parallel to the ResultSegmentSettings-definition in the {@link ResultSet}
			 * @param key The key that result-segment got mapped to in the {@link ResultSettings}/ResultSegmentSettings
			 * @param formatIdent The Identifier of the format contained in {@link ResultSegment#getResult()}
			 * @param result The result configured by the ResultSegmentSettings in the {@link ResultSettings}
			 */
			public ResultSegment(String key, String formatIdent, Object result) {
				this.key = key;
				this.formatIdent = formatIdent;
				this.result = result;
			}
			
			/**
			 * @return The key that result-segment got mapped to in the {@link ResultSettings}/ResultSegmentSettings
			 */
			public String getKey() {
				return key;
			}
			
			/**
			 * @return The Identifier of the format contained in {@link ResultSegment#getResult()}
			 */
			public String getFormatIdent() {
				return formatIdent;
			}
			
			/**
			 * @return The result configured by the ResultSegmentSettings in the {@link ResultSettings}
			 */
			public Object getResult() {
				return result;
			}
			
			@Override
			public String toString() {
				return "ResultSegment@" + getKey() + "<" + getFormatIdent() + "> = " + getResult().toString();
			}
		}
		
	}
	

	/**{@link ResultSettings} are the definition of what is result is expected from the {@link Renderable}. 
	 * It contains an array of {@link ResultSegmentSettings} to define settings for the individual segments.
	 * This array can be accessed via {@link ResultSettings#getSegments()}
	 */
	public static class ResultSettings {
		
		private ResultSegmentSettings[] segments;

		/**Create a {@link ResultSettings}-instance
		 * @param segments The settings for the individual segments that should be rendered
		 */
		public ResultSettings(ResultSegmentSettings[] segments) {
			this.segments = segments;
		}
		
		/**
		 * @return The settings for the individual segments that should be rendered
		 */
		public ResultSegmentSettings[] getSegments() {
			return segments;
		}
		
		
		/**The {@link ResultSegment} defines a segment of the result for (For example a visual track or a channel of audio)
		 * <br>
		 * Values that make up the settings of a segment:
		 * <ul>
		 * 	<li>The segment key: It is used to find back the Segment later on in the RenderResult</li>
		 * 	<li>The segment identifier: An Identifier, which describes the what result this segment is defining 
		 * 		(For example a visual track or a channel of audio)</li>
		 * 	<li>An array of ResultFormatSettings in order of priority</li>
		 * </ul>
		 * 
		 * If the {@link Renderable} can't reach the given render-segment and (one of) the given format(s), 
		 * the render will fail (for this segment). But if the class/format specific data fail, 
		 * there will be format-specific handling 
		 * - just be sure, that the expected result has to be generated, otherwise it is strongly recommended to fail. 
		 */
		public static class ResultSegmentSettings {
			
			private String key;
			private String ident;
			private ResultFormatSettings[] rfs;

			/**Instantiate a {@link ResultSegmentSettings}-object
			 * @param key The segment-key to identify the part in the returned {@link ResultSegmentSettings}
			 * @param ident The (usually standardized) identifier for the segment (Example: The key for the default IMGC-2DVisual render-segment is:  `de.hcinc.imgc.rseg.tdvis`)
			 * @param rfs Available formats for that Result Segment in an order of priority
			 */
			public ResultSegmentSettings(String key, String ident, ResultFormatSettings[] rfs) {
				this.key = key;
				this.ident = ident;
				this.rfs = rfs;
			}
			
			/**
			 * @return The segment-key to identify the part in the returned {@link ResultSegmentSettings}
			 */
			public String getKey() {
				return key;
			}
			
			/**
			 * @return The (usually standardized) identifier for the segment (Example: The key for the default IMGC-2DVisual render-segment is:  `de.hcinc.imgc.rseg.tdvis`)
			 */
			public String getIdent() {
				return ident;
			}
			
			/**
			 * @return Available formats for that Result Segment in an order of priority
			 */
			public ResultFormatSettings[] getResultFormatSettings() {
				return rfs;
			}
			
		}
		
		
		/**Defines a possible result-format of the format of a ResultSegment.
		 * <ul>
		 * 	<li>Identifier for the result-format (for example de.hcinc.imgc.res.stdbimg)</li>
		 * 	<li>Sub-format for result (For example the format {@code de.hcinc.imgc.res.stdbimg} can have 
		 * 		the sub-format RGBA, those sub-formats can also be stacked like RGBA/HDR)</li>
		 * 	<li>Information for the result-format... a map of properties in the DataPackageable format</li>
		 * </ul>
		 * 
		 * <h1>Definition: Format/Sub-Format/FormatProperties</h1>
		 * <ul>
		 * 	<li><b>Format</b>: Basic format implementation - equivalent to an object-class 
		 * 		(Example: {@code de.hcinc.imgc.res.stdbimg})</li>
		 * 	<li><b>Sub-format</b>: Setting for the format implementation (Example: RGBA) 
		 * 		- If a sub-format is marked as supported all formats having the same sub-format are 
		 * 		promised to be imported, while all format FormatProperties have to be supported the same</li>
		 * 	<li><b>FormatProperties</b>: Properties for the format, that are saved in the DataPackageable 
		 * 		and contain custom properties regarding the specific (sub-)format</li>
		 * </ul>
		 */
		public static class ResultFormatSettings {
			
			private String format;
			private String[] subFormats;
			private DataPackageable formatData;

			/**Instantiate a {@link ResultFormatSettings}-object
			 * @param format The Identifier of the Format defined
			 * @param subFormats An array that is defining the sub-format
			 * @param formatData Format-specific data
			 */
			public ResultFormatSettings(String format, String[] subFormats, DataPackageable formatData) {
				this.format = format;
				this.subFormats = subFormats;
				this.formatData = formatData;
			}
			
			/**
			 * @return The Identifier of the Format defined
			 */
			public String getFormat() {
				return format;
			}
			
			/**
			 * @return An array that is defining the sub-format
			 */
			public String[] getSubFormats() {
				return subFormats;
			}
			
			/**
			 * @return Format-specific data
			 */
			public DataPackageable getFormatData() {
				return formatData;
			}
			
		}
		
		
	}
	
	
	/**{@link RenderableData} supplies information about a {@link Renderable}. (For example the resolution for images, the sample-rate for audio or the range of drivers)
	 * The object {@link RenderableData} contains a map of properties of the {@link Renderable} that can be read by other operations such as a {@link Target} and other another {@link Renderable}.
	 * The map is keyed by strings which are recommended to look identifier-like .. the the values can be user-defined and are implemented as {@link DataPackageable} to make portability possible.
	 *
	 */
	public static class RenderableData {

		private Map<String, DataPackageable> dataMap;

		/**Instantiate a {@link DataPackageable}
		 * @param dataMap The data-map, containing {@link DataPackageable}s mapped to {@link String} keys
		 */
		public RenderableData(Map<String, DataPackageable> dataMap) {
			this.dataMap = dataMap;
		}
		
		/**Get a {@link DataPackageable} by key
		 * @param key The {@link String}-key the {@link DataPackageable} is mapped to
		 * @return The {@link DataPackageable} that is mapped, returning {@code null} if not found
		 */
		public DataPackageable get(String key) {
			DataPackageable dp = dataMap.get(key);
			if (dp != null) {
				return dp;
			} else {
				return null;
			}
		}
		
		/**Get the {@link RenderableData} as a Full JSON to export
		 * @return A {@link JSONObject} containing the data
		 */
		public JSONObject getJson() {
			
			JSONObject dataObj = new JSONObject();
			
			for (Map.Entry<String, DataPackageable> entry  : dataMap.entrySet()) {
				
				try {
					
					String key = entry.getKey();
					DataPackageable dp = entry.getValue();
					if (key != null || dp != null) {
						
						JSONObject entryObj = new JSONObject();
						
						entryObj.put("ident", dp.getIdent());
						
						String[][] packedData = dp.getData();
						
						for (String[] dataSet : packedData) {
							entryObj.put(dataSet[0], dataSet[1]);
						}
						
						dataObj.put(key, entryObj);
						
					} else {
						throw new IllegalArgumentException("WARN: RenderableData missing the key or the DataPackageable in the map");
					}
				
				} catch (Exception ex) {
					System.err.println("ERROR: The Exception \"" + ex.getClass() + "\" occurred exporting a DataPackageable-map-entry: " + ex.getMessage());
					ex.printStackTrace(System.err);
				}
				
			}
			
			return dataObj;
		}
		
	}


}
