package de.hcinc.torasu.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.hcinc.torasu.core.DataPackageable.DataPackageableMask;
import de.hcinc.torasu.core.RenderContext.RenderContextObject;
import de.hcinc.torasu.core.RenderContext.RenderModifier;
import de.hcinc.torasu.pipeline.ExecutionInterface;
import de.hcinc.torasu.util.DataUtils.Pair;

/**The {@link RenderContextMask} can...
 * <ul>
 * <li>...provide which part of the {@link RenderContext} is needed for (a) certain operation(s). 
 * 		Here it is mostly optional and should just provide a chance to stop the 
 * 		{@link RenderContext} from becoming unreasonable big.</li>
 * <li>...be used to mark for which RenderContexts a result is valid or can be reused.</li>
 * </ul>
 * 
 * Typically the use of the {@link RenderContextMask} is never mandatory, 
 * since it is just a information tool to determine on what data can be left away or which results can be reused.
 * <ul>
 * <li>It basically contains a map what RenderContextValues are relevant or what RenderContextValues it applies to. 
 * 		All not listed values are assumed as irrelevant.</li>
 * <li>The map has string keys which are the same as how the {@link RenderContextObject RenderContextObjects}/RenderContextValues 
 * 		are mapped in the RenderContext that should be masked.</li>
 * <li>The values in the map define how the mask applies on that key. The Object used for is a {@link DataPackageableMask}.</li>
 * </ul>
 * 
 * Tools it provides:
 * <ul>
 * 	<li>Masking a {@link RenderContext}, so it only only contains the the masked values</li>
 * 	<li>Check on a {@link RenderContext} is included in that mask (for example used in validity-checking, be aware that it is not checked for {@link RenderModifier}, since the {@link RenderContextMask} only contains information about the objects/values and not the modifiers)</li>
 * 	<li>The mask-map can be output</li>
 * </ul>
 * 
 * Masking Scenarios (from {@link DataPackageableMask}):
 * <ul>
 * 	<li>{@link DataPackageableMask#MASK_INCLUDED Included} (2): The data is inside the the defined region
 * 		<ul>
 * 		<li>Masking: The data is going to be included</li>
 * 		<li>Checking: The data is valid regarding that value</li>
 * 		</ul>
 * 	</li>
 * 	<li>{@link DataPackageableMask#MASK_UNDECTABLE Undetectable} (1): The compared data couldn't be absolutely detected as inside and or outside the defined region
 * 		<ul>
 * 		<li>Masking: The data is going to be included</li>
 * 		<li>Checking: The data is not seen as valid regarding that value</li>
 * 		</ul>
 * 	</li>
 * 	<li>{@link DataPackageableMask#MASK_EXCLUDED Excluded} (0): The compared data is not intersecting with the defined region
 * 		<ul>
 * 		<li>Masking: The data is allowed to be excluded</li>
 * 		<li>Checking: The data is not seen as valid regarding that value</li>
 * 		</ul>
 * 	</li>
 * 	<li>{@link DataPackageableMask#MASK_UNMASKED Unmasked} (3): There is no {@link DataPackageableMask} provided for one or more keys in the {@link RenderContext}
 * 		<ul>
 * 		<li>Masking: The data is allowed to be excluded</li>
 * 		<li>Checking: The data is valid regarding that value</li>
 * 		</ul>
 * 	</li>
 * 	<li>Error (&lt;0): An error occurred
 * 		<ul><li>Same behavior as for Undetectable
 * 		<i>(Note it is rather recommended to throw an exception if possible)</i>
 * 		</li></ul>
 * 	</li>
 * </ul>
 * 
 * <sub>from docs-guideline file (a42fabbaee3b5994065b470295e7af48da1e18c0)</sub>
 * 
 * @see RenderContext
 *
 */
public class RenderContextMask {

	public static final RenderContextMask NONE = new RenderContextMask();
	
	private HashMap<String, DataPackageableMask> maskMap;

	@SafeVarargs
	public RenderContextMask(Pair<String, DataPackageableMask>...masks) {
		this.maskMap = new HashMap<String, DataPackageableMask>();
		for (Pair<String, DataPackageableMask> entry : masks) {
			maskMap.put(entry.getValue(), entry.getExtra());
		}
	}
	
	public RenderContextMask(HashMap<String, DataPackageableMask> maskMap) {
		this.maskMap = maskMap;
	}
	
	/**Masks the RenderContext to just the needed values.
	 * @param rctx The input {@link RenderContext} to be masked
	 * @param ei {@link ExecutionInterface} that will be used for value-unpacking
	 * @param Maximum effort for value-unpacking (see {@link ExecutionInterface#unpackRenderContextObject(RenderContextObject, double)})
	 * 
	 * @return The masked {@link RenderContext}
	 */
	public RenderContext maskRCTX(RenderContext rctx, ExecutionInterface ei, double effort) {
		
		Set<Entry<String, RenderContextObject>> originalObjects = rctx.getValueObjects().entrySet();
		
		HashMap<String, RenderContextObject> newObjects = new HashMap<String, RenderContextObject>();
		
		for (Entry<String, RenderContextObject> entry : originalObjects) {
			
			DataPackageableMask valueMask = maskMap.get(entry.getKey());
			
			if (valueMask == null) {
				// Mask away since there was no entry in the mask
				continue;
			}
			
			int global = valueMask.global();
			
			if (global == DataPackageableMask.MASK_INCLUDED) {
				// Contain since value-mask said it's always included
				newObjects.put(entry.getKey(), entry.getValue());
				continue;
			} else if (global == DataPackageableMask.MASK_EXCLUDED) {
				// Mask away since value-mask said explicitly it's always excluded
				continue;
			}
			
			// unpack value for closer examination
			
			RenderContextObject unpacked;
			if (effort > 0) { // Skip unpacking for just local unpacking, since everything of that should have already been unpacked
				unpacked = ei.unpackRenderContextObject(entry.getValue(), 0);
			} else {
				unpacked = entry.getValue();
			}
			
			if (unpacked.getTransformations().length == 0) {
				
				int maskStatus = maskMap.get(entry.getKey()).check(unpacked.getValue());
				
				if (maskStatus == DataPackageableMask.MASK_INCLUDED || maskStatus == DataPackageableMask.MASK_UNDETECTABLE) {
					// Include since value mask said that value is included or undetectable
					newObjects.put(entry.getKey(), unpacked);
					continue;
				} else {
					// Exclude since value mask said that value is not included
					continue;
				}
				
			}
			
			// Include since the masking couldn't be resolved on other ways
			newObjects.put(entry.getKey(), unpacked);
			
		}
		
		// Return masked values, modifiers stay the same
		return new RenderContext(newObjects, rctx.getModifiers());
	}
	
	/**Checks if the mask is containing the values of the given {@link RenderContext}
	 * 
	 * @param rctx The given {@link RenderContext} to be checked
	 * @param ei {@link ExecutionInterface} to unpack values from {@link RenderContextObject}s
	 * 
	 * @return The coverage of the mask on the {@link RenderContext}:
	 * 	<ul>
	 * 		<li>{@link DataPackageableMask#MASK_EXCLUDED Excluded} (0): The RenderContext is not completely covered by the mask</li>
	 * 		<li>{@link DataPackageableMask#MASK_UNDECTABLE Undetectable} (1): The mask contains unpredictable behavior, no complete masking guaranteed</li>
	 * 		<li>{@link DataPackageableMask#MASK_INCLUDED Included} (2): The mask is covering the {@link RenderContext} completely</li>
	 * 		<li>{@link DataPackageableMask#MASK_UNMASKED Unmasked} (3): The {@link RenderContext} contains values that aren't mentioned in the {@link RenderContextMask}, 
	 * 			but values that are mentioned in the mask are covered by it</li>
	 * 		<li>Error (&lt;0): An error occurred (An error can also be issued over a thrown exception)</li>
	 * 	</ul>
	 */
	public int maskCheck(RenderContext rctx, ExecutionInterface ei) {

		Map<String, RenderContextObject> values = rctx.getValueObjects();
		
		// Counter for how many RenderContextObjects were mentioned in mask
		int rctxApplied = 0;
		
		int currentCheckStatus = DataPackageableMask.MASK_INCLUDED;

		int thisCheckStatus;
		
		for (Entry<String, DataPackageableMask> valueMask : maskMap.entrySet()) {
			
			thisCheckStatus = valueMask.getValue().global();
			
			RenderContextObject value = values.get(valueMask.getKey());

			if (value != null) {
				rctxApplied++; // Count RenderContextObject that has been mentioned in the mask
			}

			if (thisCheckStatus != DataPackageableMask.MASK_EXCLUDED && thisCheckStatus != DataPackageableMask.MASK_INCLUDED) { // Skip further checks, if already managed gloablly
				
				if (value != null) {
					thisCheckStatus = valueMask.getValue().check(ei.unpackRenderContextValue(value));
				} else {
					thisCheckStatus = valueMask.getValue().check(null);
				}
				
			}
			
			switch (thisCheckStatus) {
			case DataPackageableMask.MASK_EXCLUDED: // Mask Excludes
				if (value == null) {
					break; // OK, continue
				} else {
					return DataPackageableMask.MASK_EXCLUDED; // ValueMask doesn't contain
				}
			
			case DataPackageableMask.MASK_INCLUDED: // ValueMask Includes
				break; // OK, continue
			
			case DataPackageableMask.MASK_UNDETECTABLE: // ValueMask can't detect
			case DataPackageableMask.MASK_UNMASKED: // ValueMask doesn't support that type
				// Downgrade if not already, continue
				if (currentCheckStatus == DataPackageableMask.MASK_INCLUDED) {
					currentCheckStatus = DataPackageableMask.MASK_UNDETECTABLE;
				}
				break;
			}
			
		}
		
		if (currentCheckStatus == DataPackageableMask.MASK_INCLUDED) {
			
			// Check if there are values in the RenderContext that weren't mentioned in the mask
			if (values.size() > rctxApplied) {
				return DataPackageableMask.MASK_UNMASKED;
			} else {
				return DataPackageableMask.MASK_INCLUDED;
			}
			
		} else {
			return currentCheckStatus;
		}
	}
	
	/**@return The masking map. 
	 * Keyed by the keys used in the {@link RenderContextMask}, 
	 * with a {@link DataPackageableMask} for each value,
	 * that define if the masking on that value of the `RenderContext
	 */
	public HashMap<String, DataPackageableMask> getMaskMap() {
		return maskMap;
	}
	
	
}
