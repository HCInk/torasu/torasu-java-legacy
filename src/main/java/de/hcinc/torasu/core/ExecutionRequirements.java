package de.hcinc.torasu.core;


public class ExecutionRequirements {
	
	private int requiredCPUThreads;

	public ExecutionRequirements(int requiredCPUThreads) {
		this.requiredCPUThreads = requiredCPUThreads;
	}
	
	public int getRequiredCPUThreads() {
		return requiredCPUThreads;
	}
	
}
