package de.hcinc.torasu.core;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import java.util.Stack;

import de.hcinc.torasu.core.Renderable.RenderResult;
import de.hcinc.torasu.pipeline.ExecutionInterface;
import de.hcinc.torasu.util.DataUtils;

/**The {@link RenderContext} defines the situation, a render is in. It can describe basically anything concerning the context of a render.
 * A few examples are: time, shutter setting, lens, a user defined variable/color, etc...
 * <br>
 * The render context splits into two datasets;
 *
 * <ul>
 * 	<li>The <b>RenderContextValue-Map</b> contains attributes which are values, which are distributed through the render process 
 * 		(For example a position on a 2D-Area that has been marked or a color that has been defined). 
 * 		Those values are called RenderContextValue (which are stored as a {@link DataPackageable}),
 *  	but they are stored as {@link RenderContextObject} in the map to contain extra information. 
 * 		- The keys in that map can be user or defined by a Standard/Framework, if its defined by a standard or framework the key usually looks 
 * 		Identifier-like and if it is defined by a user it rather looks like a normal string.</li>
 * 	<li>The <b>RenderModifier-List</b> contains attributes, which are modifications on what operations are done while rendering 
 * 		(For example a 2D-Transformation that has to be added while rendering). 
 * 		Those the values here are called {@link RenderModifier} - 
 * 		Those modifications have to be promised beforehand by the {@link Renderable} (via {@link Renderable#promiseRenderModification(String[], RenderContext)}), 
 * 		otherwise they can't be applied (Note: In execution it is strongly recommended to use 
 * 		{@link ExecutionInterface#getModificationPromises(long, String[], RenderContext)}, to check the promises)
 * 		- Same is when the {@link Renderable} {@link RenderContext} promised the modifications, it has to do the modifications, otherwise the render will be invalidated. 
 * 		- Those Modifiers also only apply the {@link Renderable} that is executed and aren’t passed down as a RenderContextValue/{@link RenderContextObject}. 
 * 		The modifiers may be passed, but reviewed manually to obtain the same result as it would be applied by itself. 
 * 		An Exception are {@link Renderable}s only modify the RenderContextValue and/or the RenderableInformation and pass all other methods of the {@link Renderable} functionality over. 
 * 		- A RenderModifier that has been processed has to be returned in the {@link RenderResult} to validate the modification.</li>
 * </ul>
 * 
 * -> It can also be exported as a JSON (via {@link RenderContext#getJson()}).
 *
 */
public class RenderContext {
	
	private Map<String, RenderContextObject> valObjs;
	private RenderModifier[] mods;


	/**Instantiate a {@link RenderContext}
	 * @param valObjs The RenderContextValue-Map
	 * @param mods The RenderModifier-List
	 */
	public RenderContext(Map<String, RenderContextObject> valObjs, RenderModifier[] mods) {
		this.valObjs = valObjs;
		this.mods = mods;
	}
	
	/**Export the RenderContext as a JSON
	 * @return The exported JSON
	 */
	public String getJson() {
		throw new UnsupportedOperationException("getJson() is not implemented in RenderContext at the moment");
	}
	
	/**
	 * @return The RenderContextValue-Map
	 */
	public Map<String, RenderContextObject> getValueObjects() {
		return valObjs;
	}
	
	/**
	 * @return The RenderModifier-List
	 */
	public RenderModifier[] getModifiers() {
		return mods;
	}
	
	//TODO Javadoc
	//0 = No containment; 1 = Everything but the Modifiers are contained; 2 = All values are contained
	public int getContained(RenderContext rctx, RenderContextMask rctxMask) {
		//FIXME Do a real containment-check
		return equals(rctx) ? 2 : 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RenderContext) {
			RenderContext rxtx = (RenderContext) obj;
			return getContained(rxtx, null) == 2;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("RenderContext");

		sb.append("[");
		
		if (valObjs != null) {
			Iterator<Entry<String, RenderContextObject>> valEntrysIt = valObjs.entrySet().iterator();
			
			
			while (true) {
				sb.append(valEntrysIt.next());
				if (valEntrysIt.hasNext()) {
					sb.append(", ");
					continue;
				} else {
					break;
				}
			}
			
		}
		
		sb.append("]");
		
		if (mods != null && mods.length > 0) {
			sb.append("Mods");
			sb.append(DataUtils.formatStringArray(mods));
		}
		
		return sb.toString();
	}

	//TODO hash-code for RenderContext
	
	/**The {@link RenderContextObject} contains pipelined information about a RenderContextValue that is stored in a {@link RenderContext}
	 * The Object basically has three fields:
	 * <ul>
	 * 	<li>The field with the actual RenderContextValue, with direct values or a json-string that describes that value.</li>
	 * 	<li>An array of {@link RenderContextTransformationInformation} which describe how the value had been transformed 
	 * 		- this transformation has to be reverse-applied on the current value stored to match the result 
	 * 		- coming first in the array are the oldest transformations and at the end come the latest transformation</li>
	 * 	<li>Which kind of transformations even have to be reverse-applied. 
	 * 		Respectively which transformations are added to the array of {@link RenderContextTransformationInformation}</li>
	 * </ul>
	 * <b>IMPORTANT NOTE:</b> To obtain the RenderContextValue that is bundled, 
	 * it is strongly recommended to use the method {@link ExecutionInterface#unpackRenderContextValue(RenderContextObject)} of 
	 * the ExecutionInterface to automatically handle the transformation(s) or even decode the json, if no decoded object is present.
	 *
	 */
	public static class RenderContextObject {
		
		private DataPackageable value;
		private RenderContextTransformationInformation[] trans;
		private String[] writableTrans;
		
		/**A stack to enable easy modification of the RenderContextTransformationInformation-array.
		 * <ul>
		 * 	<li>It gets instantiated one the {@link RenderContextObject#writeTransformation(RenderContextTransformationInformation)} method is called.</li>
		 * 	<li>It gets written back to the trans-array as soon {@link RenderContextObject#getTransformations()} is called.</li>
		 * <ul>
		 */
		private Stack<RenderContextTransformationInformation> transStack = null;

		public RenderContextObject(DataPackageable value, String[] writableTrans, RenderContextTransformationInformation...trans) {
			this.value = value;
			this.writableTrans = writableTrans;
			this.trans = trans;
		}
		
		/**Get the un-applied bundled RenderContextValue from the RenderContextObject
		 * <br>
		 * <b>IMPORTANT NOTE:</b> To obtain the RenderContextValue that is bundled, 
		 * it is strongly recommended to use the method {@link ExecutionInterface#unpackRenderContextValue(RenderContextObject)} of 
		 * the ExecutionInterface to automatically handle the transformation(s) or even decode the json, if no decoded object is present.
		 * @return The un-applied bundled RenderContextValue
		 */
		public DataPackageable getValue() {
			return value;
		}
		
		/**
		 * @return The Identifier for the type of the bundled RenderContextValue
		 */
		public String getIdent() {
			return value.getIdent();
		}
		
		/**
		 * @return An array of the identifiers of transformations that should be written
		 */
		public String[] getWritableTransformations() {
			return writableTrans;
		}
		
		/**
		 * @return Transformations which are pending to be applied
		 */
		public RenderContextTransformationInformation[] getTransformations() {
			//Check if there is data to write back
			if (transStack != null) {
				//Write "transStack" back to "trans"
				int newSize = transStack.size();
				trans = new RenderContextTransformationInformation[newSize];
				for (int i = 0; i < newSize; i++) {
					trans[i] = transStack.get(i);
				}
				transStack = null;
			}
			
			return trans;
		}
		
		/**Write a transformation here, if noted in the array of getWritableTransformations() contains the ident of the transformation done.
		 * <br>
		 * <i>Info: There is no check of the added transformation if it is part of the writable transformations</i>
		 * @param nTrans The transformation that has to be written
		 */
		public void writeTransformation(RenderContextTransformationInformation nTrans) {
			//Check if a writable stack already exists
			if (transStack == null) {
				transStack = new Stack<RenderContextTransformationInformation>();
				for (int i = 0; i < trans.length; i++) {
					transStack.push(trans[i]);
				}
				trans = null;
			}
			transStack.push(nTrans);
		}
		
		/**Generates the JSON data to symbolize the current RenderContextObject
		 * @return The Generated JSON Object
		 */
		public String getJson() {
			throw new UnsupportedOperationException("getJson() is not implemented in RenderContextObject at the moment");
		}
		
		@Override
		public String toString() {
			if (trans.length <= 0) {
				return value.toString();
			} else {
				return "{T" + DataUtils.formatStringArray(trans) + "=>" + value + "}";
			}
		}
	}
	
	public static interface RenderContextTransformationInformation {

		//TODO Implement RenderContextTransformationInformation
		
		
		public static interface RenderContextTransformationInformationImporter {
			
			//TODO Implement RenderContextTransformationInformationImporter
		
		}
		
	}
	
	
	public static interface RenderModifier extends DataPackageable {

		//TODO Implement RenderModifier
		
		
		public static interface RenderModifierImporter {

			//TODO Implement RenderModifierImporter
			
		}
		
	}
	
}
