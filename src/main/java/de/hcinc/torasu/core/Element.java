package de.hcinc.torasu.core;

import java.util.Map;

import de.hcinc.torasu.core.Dependence.DependenceStack;
import de.hcinc.torasu.core.RenderContext.RenderModifier;
import de.hcinc.torasu.pipeline.ExecutionInterface;
import de.hcinc.torasu.pipeline.SystemResourceInterface;

public interface Element extends Cloneable {
	
	/**
	 * @return Name tag of the element for user-orientation
	 */
	public String getName();
	
	/**
	 * @return Multi-line info for user-orientation
	 */
	public String[] getInfo();

	/**Get what values of a {@link RenderContext} are going to get used in certain methods
	 * 
	 * @param operations The operations that will be executed on the Element
	 * @return The {@link RenderContextMask} that defines what part of the {@link RenderContext} is required 
	 * ({@code null} if there is no mask provided / all values have to be shipped in the {@link RenderContext})
	 */
	public RenderContextMask getRenderContextMask(String[] operations);
	
	/**Create a session to make the Element ready, referenced by the {@code readyId}
	 * (remove the session's data via {@link Element#unready(long)})
	 * 
	 * @param rctx The {@link RenderContext} it should be made ready for
	 * @param operations The operations the ready-session should make ready
	 * @return >=0: The {@code readyId}; -2 = Nothing to make ready; -1: Internal Error
	 * 
	 * @see Element#unready(long)
	 */
	public long readySession(RenderContext rctx, String[] operations);
	
	/**Update the {@link RenderContext} inside the ready-session (created via {@link Element#readySession(RenderContext, String[])})
	 * 
	 * @param readyId The {@code readyId} obtained while creating the ready-session
	 * @param rctx The updated {@link RenderContext}
	 * @param updateValues If the values got updated; true = Updated the values of the {@link RenderContext} updated; false = Only {@link RenderModifier}s got updated
	 */
	public void readySessionUpdate(long readyId, RenderContext rctx, boolean updateValues);
	
	/**Making the Element "ready" - eg. load data
	 * 
	 * @param readyId The {@code readyId} of ready-session that was started beforehand
	 * @param pld The pre-load-depth the element should be made ready at (See more at {@link Element})
	 * @param sysri The {@link SystemResourceInterface} to request resources to make the element ready
	 * @param ei {@link ExecutionInterface} for executions that are required to make the elment ready
	 * 
	 * @return The {@link ReadyRefreshPolicy}, which displays in which case the element has to be made ready again
	 */
	public ReadyRefreshPolicy readySelf(long readyId, int pld, SystemResourceInterface sysri, ExecutionInterface ei);
	
	/**Remove the {@link Element} from the state "ready" - e.g. unload data / remove all loaded data from memory
	 * 
	 * @param readyId The {@code readyId} you received on making ready via {@link Element#ready(RenderContext, String[], SystemResourceInterface, ExecutionInterface)}
	 */
	public void unready(long readyId);
	
	/**Indicates if the {@link Element} is ready (should always be true after the dependencies are worked down as described and no error was caused while making ready)
	 * 
	 * @param rctx {@link RenderContext} that should be checked for
	 * @param operations Operations that should to be checked for
	 */
	public boolean isReady(RenderContext rctx, String[] operations);
	
	/**Fetches dependencies of the {@link Element}
	 * 
	 * @param readyId The {@code readyId} of ready-session that was started beforehand
	 * @param weightIndex The {@code weightIndex} of what should be returned (see {@link Element} for more information)
	 * @param index The index of the dependence cascade (starts at 0)
	 * @param ei The {@link ExecutionInterface} for executions that have to be done for the dependence evaluation
	 * 
	 * @return The {@link DependenceStack}
	 */
	public DependenceStack dependencies(long readyId, double weightIndex, int index, ExecutionInterface ei);
	
	//Making clone public
	public Object clone() throws CloneNotSupportedException;

	/**The {@link ReadyRefreshPolicy} defines when a certain data set has to has to be refreshed or a certain action has to be re-run 
	 */
	public static class ReadyRefreshPolicy {
		
		public static final ReadyRefreshPolicy NEVER = new ReadyRefreshPolicy(Double.NaN, false, false);
		
		private double nextHigherWI;
		private boolean onValue;
		private boolean onModification;
		
		/**Instantiate a {@link ReadyRefreshPolicy}-object
		 * @param nextHigherWI The next higher {@code weightIndex} (or contained {@code pld}) it should be re-ran/refreshed for
		 * @param onValue Re-run/refresh if a value in the {@link RenderContext} has changed
		 * @param onModification Re-run/refresh if a {@link RenderModifier} in the {@link RenderContext} has changed
		 */
		public ReadyRefreshPolicy(double nextHigherWI, boolean onValue, boolean onModification) {
			this.nextHigherWI = nextHigherWI;
			this.onValue = onValue;
			this.onModification = onModification;
		}

		/**
		 * @return The next higher {@code weightIndex} (or contained {@code pld}) it should be re-ran/refreshed for
		 * 
		 * @see Element
		 */
		public double getNextHigherWI() {
			return this.nextHigherWI;
		}
		
		/**
		 * @return Re-run/refresh if a value in the {@link RenderContext} has changed
		 */
		public boolean isOnValue() {
			return onValue;
		}
		
		/**
		 * @return Re-run/refresh if a {@link RenderModifier} in the {@link RenderContext} has changed
		 */
		public boolean isOnModification() {
			return onModification;
		}
		
	}
	
	/**Provides an interface for the {@link Element} to edit/override the content of an element. 
	 * <p>
	 * (Note: If the element is an IDable the version of the element should increment on the next access, 
	 * except a version got set afterwards by {@link IDable#setVersion(long)})
	 * </p>
	 * 
	 * @see Element
	 */
	public static interface Rewritable extends Element {
		
		/**Rewrites the element-data
		 * 
		 * @param data New data of an {@link Element}
		 * @param elements The elements that should be set in the {@link Element}
		 */
		public void rewrite(DataPackageable data, Map<String, Element> elements);
		
		/**Replaces an {@link Element} inside the {@link Element}
		 * 
		 * @param key The key that defines the field where the {@link Element} should go
		 * @param elem The {@link Element} that has to be set - {@code null} to remove it
		 * 
		 * @return A status code: 0: OK; <0: Error
		 */
		public int overrideElement(String key, Element elem);
	}
	
	
	/**The {@link IDable} provides an interface, which is made to re-identify an {@link Element} and its version.
	 *
	 * @see Element
	 */
	public static interface IDable extends Element {
		
		/**Get the id of the Element
		 */
		public long getID();
		
		/**Get the version of the {@link Element}
		 * @return The version number of the {@link Element} (numbered in an ascending scheme) - indicates if the objects has changed
		 */
		public long getVersion();
		
		/**Get a unique version-stack of the element (it has no special formatting, it just has to be different if the children have changed)
		 */
		public long[] getVersionSatck();
		
		/**Set the version of element 
		 * <p>
		 * <b>NOTE: This should only done by administrative functions, 
		 * as when loading a newer element-state with a certain version-number</b>
		 * </p>
		 * 
		 * @param newVers new version that has to be set
		 */
		public void setVersion(long newVers);
		
	}
	
}
