package de.hcinc.torasu.core;

import org.json.JSONObject;

/**<ul>
 * 	<li>A DataPackageable is an object that can be packed (and ideally also unpacked by a {@link DataPackageableImporter})</ul>
 * 	<li>DataPackagables have methods to pack all the data necessary to construct a Object that behaves exactly the same externally. (In a JSON format)</ul>
 * 	<li>The types of a DataPackageable are referenced by an unique Identifier, which is also the same as the importer used to materialize the object again.</ul>
 * 	<li>Made cloneable via a {@link Cloneable} implementation</ul>
 * </ul>
 *
 * @see DataPackageableImporter
 * 
 */
public interface DataPackageable extends Cloneable {
	
	/**Get the Identifier of the {@link DataPackageable} and its {@link DataPackageableImporter}
	 */
	public String getIdent();
	
	/**Get the data to reconstruct the object - Returns a 2D map of the data to be packed
	 * <h1>
	 * How the Map (String[][]) , returned by getData() is formatted:
	 * </h1>
	 * <ul>
	 * <li>{@code String[][x]} - A Key/Value-Pair a map - {@code x} is the index here, it has no data-wise relevance</l1>
	 *
	 * <li>{@code String[0][x]} - The key of the pair
	 * 	<li>Reserved keys: {@code key} and {@code ident}</li>
	 * 	<li>Make sure to stick to the ASCII charset to improve compatibility</li>
	 * </li>
	 * <li>String[1][x] - The value of pair
	 *
	 * 	<li>Possible formats
	 *
	 * 	  <li>Surround with {@code ""} for text</li>
	 * 	  <li>Surround with {@code {}} for a JSON-object</li>
	 * 	  <li>Surround with {@code []} for a JSON-array</li>
	 * 	  <li>Use syntax {@code XX.xx}, {@code XX.} or {@code .xx} for doubles ({@code XX}/{@code xx} stands here for a sequence of integers)</li>
	 * 	  <li>Just a decimal sequence for integers</li>
	 *	</li>
	 *
	 * 	<li>Make sure to stick to the ASCII charset to improve compatibility (if there are special characters, make sure to escape them correctly in a JSON-like manner)</li>
	 * </li>
	 * </ul>
	 */
	public String[][] getData();
	
	
	/**Tool to import a string/json to a {@link DataPackageable}
	 */
	public static interface DataPackageableImporter {
		
		/**Get the Identifier unique to the object/data structure that will be imported
		 */
		public String getIdent();
		
		/**Import the data structure, it may still contain they {@code key} and {@code ident} mapped value, but it may lead to undefined behavior if accessed. 
		 * Moreover that a check if the Identifier is correct will be done from outside and how the object is mapped outside over {@code key} shouldn’t effect the behavior of the generated object
		 * 
		 * @param The {@link JSONObject} that contains the data
		 * 
		 * @return The imported {@link DataPackageable} object
		 * 
		 */
		public DataPackageable importObject(JSONObject json);
		
		
	}
	
	/**Defines a mass of {@link DataPackageable}
	 *
	 */
	public static interface DataPackageableMask {

		/**The compared data is not intersecting with the defined region
		 * <ul>
		 * 	<li>Masking: The data is allowed to be excluded</li>
		 * 	<li>Checking: The data is not seen as valid regarding that value</li>
		 * </ul>
		 */
		public static final int MASK_EXCLUDED = 0;
		
		/**The compared data couldn't be absolutely detected as inside and or outside the defined region
		 * <ul>
		 * 	<li>Masking: The data is going to be included</li>
		 * 	<li>Checking: The data is not seen as valid regarding that value</li>
		 * </ul>
		 */
		public static final int MASK_UNDETECTABLE = 1;
		
		/**The data is inside the the defined region
		 * <ul>
		 * 	<li>Masking: The data is going to be included</li>
		 * 	<li>Checking: The data is valid regarding that value</li>
		 * </ul>
		 */
		public static final int MASK_INCLUDED = 2;
		
		/**There is no DataPackageableMask provided for one or more keys
		 * <ul>
		 * 	<li>Masking: The data is allowed to be excluded</li>
		 * 	<li>Checking: The data is valid regarding that value</li>
		 * </ul>
		 */
		public static final int MASK_UNMASKED = 3;
		
		/**Checks the mask for a certain value
		 * 
		 * @param value The value to be checked
		 * 
		 * @return The containment status the current value inside the mask:<br>
		 * {@link DataPackageableMask#MASK_EXCLUDED Excluded} (0),
		 * {@link DataPackageableMask#MASK_UNDETECTABLE Undetectable} (1), 
		 * {@link DataPackageableMask#MASK_INCLUDED Included} (2), 
		 * {@link DataPackageableMask#MASK_UNMASKED Unmasked} (3)
		 * or Error (&lt;0)<br>
		 * <i>(Note: It is rather advised to throw in exception instead)</i>
		 * 
		 * @see RenderContextMask
		 */
		public int check(DataPackageable value);
		
		/**Get a global containment assumption without having to give a concrete value
		 * 
		 * @return The containment assumption:<br>
		 * <ul>
		 * <li>{@link DataPackageableMask#MASK_EXCLUDED Excluded} (0) - never contained</li>
		 * <li>{@link DataPackageableMask#MASK_UNDETECTABLE Undetectable} (1) - no absolute containment</li> 
		 * <li>{@link DataPackageableMask#MASK_INCLUDED Included} (2) - always contained</li> 
		 * <li>Error (&lt;0)<br>
		 * 		<i>(Note: It is rather advised to throw in exception instead)</i></li>
		 * </ul>
		 */
		public int global();
		

		public static final DataPackageableMask MASK_ALL_INCLUDED = new DataPackageableMask() {

			@Override
			public int check(DataPackageable value) {
				return DataPackageableMask.MASK_INCLUDED;
			}

			@Override
			public int global() {
				return DataPackageableMask.MASK_INCLUDED;
			}
			
		};
		
		public static final DataPackageableMask MASK_ALL_UNDETECTABLE = new DataPackageableMask() {

			@Override
			public int check(DataPackageable value) {
				return DataPackageableMask.MASK_UNDETECTABLE;
			}

			@Override
			public int global() {
				return DataPackageableMask.MASK_UNDETECTABLE;
			}
			
		};
		
		
	}
}
