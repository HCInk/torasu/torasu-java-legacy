package de.hcinc.torasu.core;

import de.hcinc.torasu.core.Element.ReadyRefreshPolicy;
import de.hcinc.torasu.util.DataUtils;

public class Dependence {
	
	private Element elem;
	private RenderContext rctx;
	private String[] ops;
	private int pld;

	/**A dependence of an {@link Element} in a certain state
	 * @param elem The {@link Element} that the dependence refers to
	 * @param rctx The {@link RenderContext} the {@link Element} should be ready for
	 * @param ops The array of operations the {@link Element} should be ready for
	 */
	public Dependence(Element elem, RenderContext rctx, String[] ops) {
		this.elem = elem;
		this.rctx = rctx;
		this.ops = ops;
		this.pld = 0;
	}
	
	/**A dependence of an {@link Element} in a certain state
	 * @param elem The {@link Element} that the dependence refers to
	 * @param rctx The {@link RenderContext} the {@link Element} should be ready for
	 * @param ops The array of operations the {@link Element} should be ready for
	 * @param pld The required pre-load-depth of the element
	 */
	public Dependence(Element elem, RenderContext rctx, String[] ops, int pld) {
		this.elem = elem;
		this.rctx = rctx;
		this.ops = ops;
		this.pld = pld;
	}
	
	/**Get the {@link Element} that the dependence refers to
	 */
	public Element getElement() {
		return elem;
	}
	
	/**@return The {@link RenderContext} the {@link Element} should be ready for
	 */
	public RenderContext getRenderContext() {
		return rctx;
	}
	
	/**Get the array of operations the {@link Element} should be ready for
	 * 
	 * @return An array of identifiers of the operations, when {@code null} is returned all operations have to be made ready
	 */
	public String[] getOperations() {
		return ops;
	}
	
	/**@return The minimum pre-load-depth the {@link Element} should be ready for
	 */
	public int getPld() {
		return pld;
	}
	
	/**Calculates equalities between another dependence<br>
	 * -1 = No containment in the RenderContext, but pld not;<br>
	 * -2 = Everything but the Modifiers are contained in the RenderContext, but pld not;<br>
	 * -3 = Render Context Contained, but pld not;<br>
	 * 0 = No containment in the RenderContext, pld is okay;<br>
	 * 1 = Everything but the Modifiers are contained in the RenderContext, pld is okay;<br>
	 * 2 = Everything is contained<br>
	 */
	//FIXME doesnt check operations
	public int getContained(Dependence def, RenderContextMask rctxMask) {
		//RCTX: 0 = No containment; 1 = Everything but the Modifiers are contained; 2 = All values are contained
		int rctxContainment = def.getRenderContext().getContained(def.getRenderContext(), rctxMask);

		if (pld < def.getPld()) { // Check if pld is contained
			return rctxContainment; //Contained (0,1,2)
		} else {
			return rctxContainment*-1-1; //Contained (-1,-2,-3)
		}
		
	}

	@Override
	public String toString() {
		return "Dependence{" + elem + " w/ ops" + DataUtils.formatStringArray(ops) + " @ " + rctx + "}";
	}
	
	//TODO Custom comparison and hash?
	
	/**(Partial) Dependencies emitted by an element
	 *
	 */
	public static class DependenceStack {
		
		private int index;
		private int size;
		private ReadyRefreshPolicy srp;
		private ReadyRefreshPolicy frp;
		private Dependence[] dependencies;

		/**Create a {@link DependenceStack} (only for index>0)<br>
		 * NOTE: Use for the first of the cascade (index=0) 
		 * {@link DependenceStack#DependenceStack(int, ReadyRefreshPolicy, ReadyRefreshPolicy, Dependence...)}
		 * 
		 * @param index The index for a cascaded dependence (set index=0 size=1 if you are not using cascaded dependencies) 
		 * @param size The total count of the dependence cascade
		 * @param srp The {@link ReadyRefreshPolicy} for the currentStack
		 * @param dependencies The dependencies to be made ready (in this step)
		 */
		public DependenceStack(int index, int size, ReadyRefreshPolicy srp, Dependence...dependencies) {
			this.index = index;
			this.size = size;
			this.srp = srp;
			this.frp = null;
			this.dependencies = dependencies;
		}
		
		/**Create a {@link DependenceStack} 
		 * for the first cascade (index=0)
		 * 
		 * @param size The total count of the dependence cascade 
		 * @param frp The {@link ReadyRefreshPolicy} for the whole cascade 
		 * 			(cascade size also may change - only contained at index=0)
		 * @param srp The {@link ReadyRefreshPolicy} for the currentStack
		 * @param dependencies The dependencies to be made ready (in this step)
		 */
		public DependenceStack(int size, ReadyRefreshPolicy frp, ReadyRefreshPolicy srp, Dependence...dependencies) {
			this.index = 0;
			this.size = size;
			this.frp = frp;
			this.srp = srp;
			this.dependencies = dependencies;
		}
		
		/**Make a simple {@link DependenceStack} without cascading
		 * @param rp {@link ReadyRefreshPolicy} where the dependencies has to be refreshed
		 * @param dependencies The dependencies that have to be put into the {@link DependenceStack}
		 * @return The generated {@link DependenceStack}
		 */
		public static DependenceStack makeSimpleStack(ReadyRefreshPolicy rp, Dependence...dependencies) {
			return new DependenceStack(1, rp, rp, dependencies);
		}
		
		/**
		 * @return The index of the current cascade (index starts at 0)
		 */
		public int getIndex() {
			return index;
		}
		
		/**
		 * @return How many cascades are there in total
		 */
		public int getSize() {
			return size;
		}
		
		/**
		 * @return The {@link ReadyRefreshPolicy} for the current stack
		 */
		public ReadyRefreshPolicy getStackRefreshPolicy() {
			return srp;
		}
		
		/**
		 * @return The {@link ReadyRefreshPolicy} for the whole cascade 
		 * (cascade size also may change - only contained at index=0)
		 */
		public ReadyRefreshPolicy getFullRefreshPolicy() {
			return frp;
		}
		
		/**
		 * @return dependencies in the current stack
		 */
		public Dependence[] getDependencies() {
			return dependencies;
		}
		
		
		
	}
}
