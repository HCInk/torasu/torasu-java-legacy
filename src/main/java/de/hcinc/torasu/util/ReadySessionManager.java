package de.hcinc.torasu.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import de.hcinc.torasu.core.RenderContext;

//TODO javadoc
public class ReadySessionManager {
	
	/**Map of the ready sessions */
	Map<Long, ReadySessionObject> rsMap = new ConcurrentHashMap<Long, ReadySessionObject>();
	private long counter = 0L;
	
	public ReadySessionManager() {
		
	}
	
	public ReadySessionObject add(RenderContext rctx, String[] operations) {
		long readyId = counter;
		counter++;
		
		ReadySessionObject rso = new ReadySessionObject(readyId, rctx, operations);
		rsMap.put(readyId, rso);
		
		return rso;
	}
	
	public ReadySessionObject add(long readyId, RenderContext rctx, String[] operations) {
		ReadySessionObject rso = new ReadySessionObject(readyId, rctx, operations);
		rsMap.put(readyId, rso);
		return rso;
	}
	
	public ReadySessionObject get(long readyId) {
		return rsMap.get(readyId);
	}
	
	public boolean remove(long readyId) {
		return rsMap.remove(readyId) != null;
	}
	
	public static class ReadySessionObject {

		private long readyId;
		private RenderContext rctx;
		private String[] operations;

		public ReadySessionObject(long readyId, RenderContext rctx, String[] operations) {
			this.readyId = readyId;
			this.rctx = rctx;
			this.operations = operations;
		}
		
		public void update(RenderContext rctx, boolean changedValues) {
			this.rctx = rctx;
		}
		
		public long getReadyId() {
			return readyId;
		}
		
		public RenderContext getRenderContext() {
			return rctx;
		}
		
		public String[] getOperations() {
			return operations;
		}
	}
	
}
