package de.hcinc.torasu.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import de.hcinc.torasu.core.Element;
import de.hcinc.torasu.core.RenderContext;
import de.hcinc.torasu.core.RenderContextMask;
import de.hcinc.torasu.core.DataPackageable.DataPackageableMask;
import de.hcinc.torasu.pipeline.ExecutionInterface;

/**A {@link RenderContextMask} which is designed to also include 
 * the masks of other {@link Element Elements}<br>
 * <i>Recommended for generating a {@link RenderContextMask} 
 * for {@link Element#getRenderContextMask(String[])}</i>
 */
public class ElementRenderContextMask extends RenderContextMask {

	private RenderContextMask baseMask;
	private MaskInclude[] includes;

	/**The cache of the built {@link RenderContextMask}*/
	private RenderContextMask madeMask = null;
	
	/**Creates an {@link ElementRenderContextMask} which only contains a {@link RenderContextMask}
	 * 
	 * @param baseMask The given {@link RenderContextMask} to be displayed
	 */
	public ElementRenderContextMask(RenderContextMask baseMask) {
		this.baseMask = baseMask;
		this.includes = new MaskInclude[0];
	}
	
	/**Creates an {@link ElementRenderContextMask} which merges the 
	 * {@link RenderContextMask RenderContextMasks} from the given 
	 * {@link Element}-includes ({@link MaskInclude})
	 * 
	 * @param includes The includes to be merged
	 */
	public ElementRenderContextMask(MaskInclude...includes) {
		this.baseMask = null;
		this.includes = includes;
	}
	
	/**Creates an {@link ElementRenderContextMask} which merges the 
	 * {@link RenderContextMask RenderContextMasks} from the given 
	 * base mask and the given {@link Element}-includes ({@link MaskInclude})
	 * 
	 * @param baseMask The base-mask to be merged
	 * @param includes The includes to be merged
	 */
	public ElementRenderContextMask(RenderContextMask baseMask, MaskInclude...includes) {
		this.baseMask = baseMask;
		this.includes = includes;
	}
	
	/**@return The base mask, the generated {@link RenderContextMask} is based on
	 */
	public RenderContextMask getBaseMask() {
		return baseMask;
	}
	
	/**@return The sub-masks that should also be included to the rest of the mask
	 */
	public MaskInclude[] getIncludes() {
		return includes;
	}
	
	/**Reset the calculated result 
	 * (To be used when the base-mask gets changed 
	 * or the included {@link Element Elements} 
	 * below change their {@link RenderContextMask})
	 */
	public synchronized void reset() {
		madeMask = null;
	}
	
	/**Ensures the mask is built<br>
	 * - Rebuild with {@link ElementRenderContextMask#reset()}
	 */
	private synchronized void makeMask() {
		if (madeMask == null) {
			
			// Quick-solutions for no-/single-mask scenarios
			if (baseMask != null) {
				if (includes.length <= 0) {
					// Quick-solution: The only mask found is the base-mask: Adopt directly the base-mask
					madeMask = baseMask;
					return;
				}
			} else {
				if (includes.length > 0) {
					if (includes.length == 1) {
						// Quick-solution: No base-mask and only one include: Adopt directly the include
						madeMask = includes[0].getElement().getRenderContextMask(includes[0].getOperations());
						return;
					}
				} else {
					// Quick-solution: No masks to be merged at all
					baseMask = RenderContextMask.NONE;
					return;
				}
			}
			
			HashMap<String, DataPackageableMask> maskMap = new HashMap<String, DataPackageableMask>();
			
			//Iterate thru includes
			for (int i = 0; i <= includes.length; i++) {
				
				
				// Mentioned Map
				HashMap<String, DataPackageableMask> includedMaskMap;
				
				// Ignored contents of the mentioned map
				HashSet<String> ignored;

				if (i > 0) {
					
					MaskInclude elementInclude = includes[i-1];
					
					includedMaskMap = elementInclude.getElement().getRenderContextMask(elementInclude.getOperations()).getMaskMap();
					ignored = elementInclude.getIgnoredValues();
					
				} else {
					if (baseMask != null) {
						
						includedMaskMap = baseMask.getMaskMap();
						ignored = new HashSet<String>(0); // No ignored values
						
					} else {
						continue; // Base-mask not set, next loop
					}
				}
				
				// Direct copy if no mask has been created yet
				if (maskMap == null) {
					maskMap = includedMaskMap;
					continue;
				}
				
				for (Entry<String, DataPackageableMask> valueInclude : includedMaskMap.entrySet()) {
					
					String key = valueInclude.getKey();
					
					if (!ignored.contains(key)) {
						DataPackageableMask existing = maskMap.get(key);
						if (existing == null) {
							// Put value, since none has been put yet
							maskMap.put(key, valueInclude.getValue());
						} else {
							// Display value as not able to merge, include merging later on
							maskMap.put(key, DataPackageableMask.MASK_ALL_UNDETECTABLE);
						}
					}
					
				}
				
				
			}
			
			madeMask = new RenderContextMask(maskMap);
			
		}
	}
	
	/**@return Built map entries (as in {@link RenderContextMask#getMaskMap()})<br>
	 * - Rebuild with {@link ElementRenderContextMask#reset()}
	 */
	@Override
	public HashMap<String, DataPackageableMask> getMaskMap() {
		makeMask();
		return madeMask.getMaskMap();
	}

	/**@return Checks the containment of the built mask (as in {@link RenderContextMask#maskCheck(RenderContext, ExecutionInterface)})<br>
	 * - Rebuild with {@link ElementRenderContextMask#reset()}
	 */
	@Override
	public int maskCheck(RenderContext rctx, ExecutionInterface ei) {
		makeMask();
		return madeMask.maskCheck(rctx, ei);
	}

	/**@return Masks the built mask (as in {@link RenderContextMask#maskRCTX(RenderContext, ExecutionInterface, double)})<br>
	 * - Rebuild with {@link ElementRenderContextMask#reset()}
	 */
	@Override
	public RenderContext maskRCTX(RenderContext rctx, ExecutionInterface ei, double effort) {
		makeMask();
		return madeMask.maskRCTX(rctx, ei, effort);
	}
	
	
	/**A declaration of a {@link RenderContextMask}-include 
	 * from an {@link Element} 
	 * - Used in {@link ElementRenderContextMask}
	 */
	public static class MaskInclude {
		
		private Element element;
		private String[] operations;
		private HashSet<String> ignoredValues;

		/**Creates a {@link MaskInclude}
		 * @param element The {@link Element} that the include is referring to
		 * @param operations The operations that have to be included from the {@link Element}
		 */
		public MaskInclude(Element element, String...operations) {
			this.element = element;
			this.operations = operations;
		}
		

		/**Creates a {@link MaskInclude}
		 * @param element The {@link Element} that the include is referring to
		 * @param ignoredValues The values from the {@link RenderContextMask} that shouldn't be included
		 * @param operations The operations that have to be included from the {@link Element}
		 */
		public MaskInclude(Element element, String[] ignoredValues, String...operations) {
			this.element = element;
			this.operations = operations;
			this.ignoredValues = new HashSet<String>(ignoredValues.length);
			for (String ignoredValue : ignoredValues) {
				this.ignoredValues.add(ignoredValue);
			}
		}
		
		/**@return The {@link Element} that the include is referring to 
		 */
		public Element getElement() {
			return element;
		}
		
		/**@return The values from the {@link RenderContextMask} that shouldn't be included
		 */
		public String[] getOperations() {
			return operations;
		}
		
		/**@return The operations that have to be included from the {@link Element}
		 */
		public HashSet<String> getIgnoredValues() {
			return ignoredValues;
		}
		
	}
	
	
}
