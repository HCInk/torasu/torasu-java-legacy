package de.hcinc.torasu.util;

public class DataUtils {
	
	public static class Pair<V, E> {
	    V value;
	    E extra;

	    public Pair(V value, E extra) {
	        this.value = value;
	        this.extra = extra;
	    }
	    
	    public V getValue() {
			return value;
		}
	    
	    public E getExtra() {
			return extra;
		}
	    
	    public void setValue(V value) {
	    	this.value = value;
	    }
	    
	    public void setExtra(E extra) {
	    	this.extra = extra;
	    }
	    
	}
	
	public static String formatStringArray(Object[] sarr) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("[");
		
		for (int i = 0; true;) {
			sb.append(sarr[i].toString());
			i++;
			if (i < sarr.length) {
				sb.append(", ");
				continue;
			} else {
				break;
			}
		}
		
		sb.append("]");
		
		return sb.toString();
	}
	
}
