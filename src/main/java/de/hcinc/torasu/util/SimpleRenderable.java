package de.hcinc.torasu.util;

import de.hcinc.torasu.core.Dependence.DependenceStack;
import de.hcinc.torasu.core.ExecutionRequirements;
import de.hcinc.torasu.core.PipelineSettings;
import de.hcinc.torasu.core.RenderContext;
import de.hcinc.torasu.core.RenderContextMask;
import de.hcinc.torasu.core.Renderable;
import de.hcinc.torasu.pipeline.ExecutionInterface;
import de.hcinc.torasu.pipeline.SystemResourceInterface;

public abstract class SimpleRenderable implements Renderable {

	protected ReadySessionManager rsm = new ReadySessionManager();

	/*
	 * 		Render
	 */
	
	// Implement render Here
	@Override
	public abstract RenderResult render(RenderWrangler rw); 
	
	/*
	 * 		Data
	 */
	
	// Add a name here
	@Override
	public abstract String getName();
	
	// Add RenderContextMask 
	@Override
	public abstract RenderContextMask getRenderContextMask(String[] segments);

	// No information attached, override to attach 
	@Override
	public String[] getInfo() {
		return null;
	}
	
	// No special data attached, override to attach
	@Override
	public RenderableData data(String[] keys, RenderContext rctx) {
		return null;
	}

	// No RenderModifiers attached, override to attach
	@Override
	public String[] promiseRenderModification(String[] requests, RenderContext rctx) {
		return null;
	}

	// Default to 1 CPU-thread, override to change
	@Override
	public ExecutionRequirements getExecutionRequirements(RenderContext rctx, String[] segments) {
		return new ExecutionRequirements(1);
	}

	// No PipelineSettings attached by default, override to change
	@Override
	public PipelineSettings getPipelineSettings() {
		return null;
	}
	
	/*
	 * Ready Stuff
	 */
	
	// Implement making ready here
	@Override
	public abstract ReadyRefreshPolicy readySelf(long readyId, int pld, SystemResourceInterface sysri, ExecutionInterface ei) ;

	
	// Implement check for ready here
	@Override
	public abstract boolean isReady(RenderContext rctx, String[] operations);
	
	// Implement cleaning here
	/**Removes unneeded stuff from a previous ready-state
	 */
	public abstract void cleanReady(long readyId);
	
	// Implement output of dependencies here
	@Override
	public abstract DependenceStack dependencies(long readyId, double weightIndex, int index, ExecutionInterface ei);
	
	// Initialize a ready-session
	public long readySession(RenderContext rctx, String[] operations) {
		return rsm.add(rctx, operations).getReadyId(); //add to rsm
	}
	
	// Update a ready-session
	public void readySessionUpdate(long readyId, RenderContext rctx, boolean updateValues) {
		rsm.get(readyId).update(rctx, updateValues); //update in rsm
	}
	
	// Remove ready-session
	@Override
	public void unready(long readyId) {
		rsm.remove(readyId); //remove from rsm
		cleanReady(readyId);
	}
	
	/*
	 * 		Cloning
	 */
	
	// Implement clone here
	@Override
	public abstract Object clone() throws CloneNotSupportedException;

}
