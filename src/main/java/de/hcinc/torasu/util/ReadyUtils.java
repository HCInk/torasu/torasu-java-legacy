package de.hcinc.torasu.util;

import java.util.Stack;

import de.hcinc.torasu.core.Dependence;
import de.hcinc.torasu.core.Dependence.DependenceStack;
import de.hcinc.torasu.core.Element;
import de.hcinc.torasu.core.RenderContext;
import de.hcinc.torasu.pipeline.ExecutionInterface;
import de.hcinc.torasu.pipeline.SystemResourceInterface;
import de.hcinc.torasu.util.DataUtils.Pair;

public class ReadyUtils {
	
	//TODO Javadoc
	public static Stack<Pair<Element, Long>> makeReadyMinimum(Element elem, RenderContext rctx, String[] ops, int pld, SystemResourceInterface sri, ExecutionInterface ei) {
		
		//Ready-instantiations
		
		Stack<Pair<Element, Long>> readySessions = new Stack<Pair<Element, Long>>();
		
		//Declared Definition
		Dependence dDef = new Dependence(elem, rctx, ops, pld);
		
		//Working Stack
		Stack<MakeReadyObject> wSatck = new Stack<MakeReadyObject>();
		
		wSatck.push(new MakeReadyObject(dDef));
		
		
		
		while (!wSatck.isEmpty()) {
			MakeReadyObject cMkRdyO = wSatck.peek();
			
			//DISPLAY STUFF
			int stackSize = wSatck.size();
			
			StringBuilder spacer = new StringBuilder();
			
			for (int i = 0; i < stackSize; i++) {
				spacer.append(" ");
			}
			
			//String dsipPref = "RDY~" + spacer.toString()  + "[" + stackSize + "]" + cMkRdyO + "::"; 

			//ACTUAL WORK
			
			
			Dependence cDef = null; 
			
			switch (cMkRdyO.getStage()) {
			
			case INIT:
				//System.out.println(dsipPref + "INIT");
				cDef = cMkRdyO.getDef();
				Element element = cDef.getElement();
				
				long readyId = element.readySession(cDef.getRenderContext(), cDef.getOperations());
				
				if (readyId >= 0) {
					cMkRdyO.setReadySessionId(readyId);
					readySessions.add(new Pair<Element, Long>(element, readyId));
					cMkRdyO.setStage(MakeReadyObject.ReadyStage.MK_DEPS);
				} else {
					if (readyId == -2) {
						//-2: No need to make ready
						//System.out.println(dsipPref + "INIT::NO NEED");
						cMkRdyO.setStage(MakeReadyObject.ReadyStage.RDY);
					} else {
						// Unknown negative code: Error
						throw new RuntimeException("Couldnt make a ready-session in " + cDef.getElement() + " - Code: " + readyId);
					}
					break;
				}
				
				// Go on straight into the make deps section
				
			case MK_DEPS:
				
				if (cDef == null) {cDef = cMkRdyO.getDef();} //Make sure an cDef insatnce is loaded
				
				int depCascadeIndex = cMkRdyO.getDepCascadeIndex();
				
				//System.out.println(dsipPref + "GET DEPS::" + depCascadeIndex);
				DependenceStack depStack = cDef.getElement().dependencies(cMkRdyO.getReadySessionId(), cDef.getPld()*-1, depCascadeIndex, ei);
				
				if (depStack != null) {
					boolean hasDeps = depCascadeIndex <= 0;
					for (Dependence cDep : depStack.getDependencies()) {
						if (!hasDeps) {hasDeps = true;}
						
						//Creation of a sub ready
						MakeReadyObject mkro = new MakeReadyObject(cDep);
						//System.out.println(dsipPref + "GET DEPS::" + depCascadeIndex + "::ADD DEP: " + cDep + " -> " + mkro);
						wSatck.push(mkro);
						
						
					}
					
					depCascadeIndex++;
					
					if (depCascadeIndex < depStack.getSize()) {
						cMkRdyO.setDepCascadeIndex(depCascadeIndex);
					} else {
						cMkRdyO.setStage(MakeReadyObject.ReadyStage.MK_RDY);
						if (hasDeps) {
							break;
						}
					}
				} else {
					cMkRdyO.setStage(MakeReadyObject.ReadyStage.MK_RDY);
				}
				
			case MK_RDY:
				if (cDef == null) {cDef = cMkRdyO.getDef();} //Make sure an cDef insatnce is loaded

				//System.out.println(dsipPref + "MAKE READY");
				
				cDef.getElement().readySelf(cMkRdyO.getReadySessionId(), cDef.getPld(), sri, ei);
				
				cMkRdyO.setStage(MakeReadyObject.ReadyStage.RDY);
				
				// Go on straight into the is ready section...
				
			case RDY:
				//System.out.println(dsipPref + "READY POP");;
				wSatck.remove(cMkRdyO);
				break;

			}
			
		}
		
		return readySessions;
	}
	
	private static class MakeReadyObject {
		ReadyStage stage = ReadyStage.INIT;
		private Dependence def;
		private int depCascadeIndex = 0;
		private long readySessionId = -1L;
		
		public MakeReadyObject(Dependence def) {
			this.def = def;
		}
		
		public void setReadySessionId(long readySessionId) {
			this.readySessionId = readySessionId;
		}

		public void setStage(ReadyStage stage) {
			this.stage = stage;
		}
		
		public void setDepCascadeIndex(int depCascadeIndex) {
			this.depCascadeIndex = depCascadeIndex;
		}
		
		public long getReadySessionId() {
			return readySessionId;
		}
		
		public ReadyStage getStage() {
			return stage;
		}
		
		public int getDepCascadeIndex() {
			return depCascadeIndex;
		}
		
		public Dependence getDef() {
			return def;
		}
		
		@Override
		public String toString() {
			return "MkRdyObj@" + Long.toHexString(System.identityHashCode(this));
		}
		
		private enum ReadyStage {
			/**Initialize things (also optionally run the first dep-cascade)
			 */
			INIT,
			/**Make the (next) dep-cascade ready
			 * (Get the index next cascade to be made ready via {@link MakeReadyObject#getDepCascadeIndex()})
			 */
			MK_DEPS,
			/**Has to be made ready next
			 */
			MK_RDY,
			/**The object has been made ready
			 */
			RDY
		}
	}
	
	
}
