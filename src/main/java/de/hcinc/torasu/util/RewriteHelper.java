package de.hcinc.torasu.util;

import java.util.Map;
import java.util.Map.Entry;

import de.hcinc.torasu.core.Element;

public class RewriteHelper {

	Map<String, ElementPointer<?>> map;
	
	public RewriteHelper(Map<String, RewriteHelper.ElementPointer<?>> map) {
		this.map = map;
	}
	
	public void overrideAll(Map<String, Element> elements) {
		
		for (Entry<String, ElementPointer<?>> entry : map.entrySet()) {
			entry.getValue().setValue(null);
		}
		
		for (Entry<String, Element> entry : elements.entrySet()) {
			overrideElement(entry.getKey(), entry.getValue());
		}
	}
	
	public void overrideElement(String key, Element elem) {
		
		ElementPointer<?> pointer = map.get(key);
		
		if (pointer != null) {
			
			Class<? extends Element> sample = pointer.getClassSample();
			if (sample.isInstance(elem)) {
				
				pointer.setValue(sample);
				
			} else {
				throw new IllegalArgumentException("The given element type " + elem.getClass() 
				+ " (as key \"" + key + "\") is not an instance of the given class " + sample);
			}
			
		} else {
			throw new IllegalArgumentException("Writing to element-slot, that doesnt exist: \"" + key + "\"");
		}
		
	}
	
	
	public static class ElementPointer<T extends Element> {
		private Class<T> classSample;
		private T value;

		public ElementPointer(Class<T> classSample, T value) {
			this.classSample = classSample;
			this.value = value;
		}
		
		public Class<T> getClassSample() {
			return classSample;
		}
		
		public T getValue() {
			return value;
		}
		
		@SuppressWarnings("unchecked")
		public void setValue(Object value) {
			this.value = (T) value;
		}
	}
}
