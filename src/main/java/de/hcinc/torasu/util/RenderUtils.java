package de.hcinc.torasu.util;

import de.hcinc.torasu.core.Renderable.RenderResult;
import de.hcinc.torasu.core.Renderable.ResultSettings;
import de.hcinc.torasu.core.Renderable.ResultSettings.ResultFormatSettings;
import de.hcinc.torasu.core.Renderable.ResultSettings.ResultSegmentSettings;

public class RenderUtils {
	
	/**Finds the key of a certain {@link ResultSegmentSettings} by its identifier
	 * 
	 * @param rs The {@link ResultSettings} of the operation
	 * @param segIdent The segment Identifier to find
	 * 
	 * @return The {@link ResultSegmentSettings} that have been found, 
	 * 			{@code null} if nothing was found
	 */
	public static ResultSegmentSettings findRSEGbyIdent(ResultSettings rs, String segIdent) {
		ResultSegmentSettings[] segments = rs.getSegments();
		for (ResultSegmentSettings cseg : segments) {
			if (cseg.getIdent().contentEquals(segIdent)) {
				return cseg;
			}
		}
		return null;
	}
	
	//TODO Javadoc
	public static String[] elemOpsFromResultSegmentSettings(ResultSegmentSettings[] rsegSettings) {
		String[] ops = new String[rsegSettings.length];
		
		for (int i = 0; i < rsegSettings.length; i++) {
			ops[i] = rsegSettings[i].getIdent();
		}
		
		return ops;
	}
	
	//TODO Javadoc
	public static RenderResult simpleResult(int status, String key, String formatIdent, Object result) {
		return new RenderResult(status, new RenderResult.ResultSegment[] {
				new RenderResult.ResultSegment(key, formatIdent, result)
		});
	}
	
	//TODO Javadoc
	public static ResultSettings simpleSettings(String key, String ident, ResultFormatSettings format) {
		return new ResultSettings(new ResultSegmentSettings[] {
					new ResultSegmentSettings(key, ident, 
							new ResultFormatSettings[] {format})
				}
			);
		
	}
	
}
