package de.hcinc.torasu.pipeline;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;

import de.hcinc.torasu.core.Element;
import de.hcinc.torasu.core.RenderContext;
import de.hcinc.torasu.core.RenderContext.RenderContextObject;
import de.hcinc.torasu.core.Renderable;
import de.hcinc.torasu.core.Renderable.RenderResult;
import de.hcinc.torasu.core.Renderable.RenderWrangler;
import de.hcinc.torasu.core.Renderable.RenderableData;
import de.hcinc.torasu.core.Renderable.ResultSettings;
import de.hcinc.torasu.core.Target;
import de.hcinc.torasu.core.Target.TargetResult;
import de.hcinc.torasu.log.LogEntry.LogLevel;
import de.hcinc.torasu.log.LogEntry.LogMessage;
import de.hcinc.torasu.log.LogInstruction;
import de.hcinc.torasu.log.LogObserver;
import de.hcinc.torasu.log.SubLogObserver;
import de.hcinc.torasu.util.DataUtils.Pair;
import de.hcinc.torasu.util.ReadyUtils;
import de.hcinc.torasu.util.RenderUtils;

public class RenderRunner implements Runnable {

	long renderIndexCounter = 0L;

	protected PriorityBlockingQueue<RenderObject> feedingQueue = new PriorityBlockingQueue<RenderObject>();
	private PriorityBlockingQueue<RenderObject> renderQueue = new PriorityBlockingQueue<RenderObject>();
	ConcurrentHashMap<Long, RenderResult> resultMap = new ConcurrentHashMap<Long, RenderResult>();
	ConcurrentHashMap<Long, RenderResult> finalResultMap = new ConcurrentHashMap<Long, RenderResult>();

	private boolean running;

	private int maxThreads;
	
	public RenderRunner(int maxThreads) {
		this.maxThreads = maxThreads;
	}
	
	//Just for debug
	private static void formatOutputLine(PrintStream ps, String text, String format) {
		String[] split = text.split("\n");
		for (int i = 0; i < split.length; i++) {
			ps.println(format + split[i] + "\033[0;m");
		}
	}

	@Override
	public void run() {
		running = true;

		int timeSleep = 0;
		int maxTimeSleep = 50;
		
		while (running) {
			
			boolean operationsDone = false;
			int threadCounter = 0;
			
			if (!renderQueue.isEmpty()) {
				//System.out.println("Render Scheduler:");
				Object[] ll = renderQueue.toArray();
				Arrays.sort(ll);
				
				for (Object obj : ll) {
					RenderObject renderObject = (RenderObject) obj;
					//System.out.println(" - rob " + renderObject); //Visualization of the render queue
					
					
					{	//If finished, push result to the fitting result map
						RenderResult result = renderObject.getRenderResult();
						
						if (result != null) {
							renderQueue.remove(renderObject); //Does removing this during iteration work?
							long renderId = renderObject.getRenderId();
							if (!renderObject.isRoot()) {
								//System.out.println("N-RES " + renderId + " - " + result);
								resultMap.put(renderId, result);
							} else {
								//System.out.println("F-RES " + renderId + " - " + result);
								finalResultMap.put(renderId, result);
							}
							operationsDone = true;
							//System.out.println("  -> Has Result. Saved result, no thread.");
							//formatOutputLine(System.out, "  -> Has Result. Saved result, no thread.", "\033[96m");
							continue;
						}
					}
	
					//Set all threads over the limit to paused
					
					if (threadCounter >= maxThreads) {
						if (renderObject.isRunning()) {
							//System.out.println("  -> Max Threads. Sleep, no thread.");
							//formatOutputLine(System.out, "  -> Max Threads. Sleep, no thread.", "\033[91m"); //Red
							renderObject.setPaused(true);
						} else {
							//System.out.println("  -> Max Threads. Dont start thread, no thread.");
							//formatOutputLine(System.out, "  -> Max Threads. Dont start thread, no thread.", "\033[93m"); //Yellow
						}
						continue;
					}
					
					if (renderObject.isRunning()) {
						
						// Resume if paused
						if (renderObject.isPaused()) {
							renderObject.setPaused(false);
						}
						
						//Look for the result, if holding
						long holding = renderObject.getHoldingStatus();
						if (holding > 0) {
							RenderResult holdResult = resultMap.remove(holding);
							if (holdResult != null) {
								int status = renderObject.insertHolding(holdResult, holding);
								if (status >= 0) {
									threadCounter++;
									operationsDone = true;
									//System.out.println("  -> Holding thread. Hold-result inserted, count thread. (" + threadCounter + ")");
									//formatOutputLine(System.out, "  -> Holding thread. Hold-result inserted, count thread. (" + threadCounter + ")", "\033[95m"); //Purple
								} else {
									//System.out.println("  -> Holding thread. Hold-result-insert failed, no thread.");
									formatOutputLine(System.err, "  -> Holding thread. Hold-result-insert failed, no thread.", "\033[97;41m"); //Red and white
									//TODO put an error that the holding was filled unexpectedly
								}
							} else {
								//System.out.println("  -> Holding thread. No hold-result available, no thread.");
								//formatOutputLine(System.out, "  -> Holding thread. No hold-result available, no thread.", "\033[90m"); //Grey
							}
						} else {
							threadCounter++;
							//System.out.println("  -> Running thread. Thread continue, count thread. (" + threadCounter + ")");
							//formatOutputLine(System.out, "  -> Running thread. Thread continue, count thread. (" + threadCounter + ")", "\033[36m"); //Cyan
						}
						
					} else {
						renderObject.start();
						threadCounter++;
						operationsDone = true;
						//System.out.println("  -> New thread. Thread started, count thread. (" + threadCounter + ")");
						//formatOutputLine(System.out, "  -> New thread. Thread started, count thread. (" + threadCounter + ")", "\033[92m");  // Green
					}
				}

			}

			//Move feed to queue
			synchronized (feedingQueue) {
				while (!feedingQueue.isEmpty()) {
					renderQueue.add(feedingQueue.poll());
				}
			}
			
			if (!operationsDone) {
				try {
					Thread.sleep(timeSleep);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (timeSleep < maxTimeSleep) {
					timeSleep++;
				} else {
					timeSleep = maxTimeSleep;
				}
			} else {
				if (timeSleep > 0) {
					timeSleep = 0;	
				}
			}
			
		}
		
	}
	
	public void stop() {
		running = false;
	}
	
	private synchronized long putRenderTask(RenderObject ro) {
		
		long renderId = renderIndexCounter;
		
		ro.setRenderId(renderId);
		
		synchronized (feedingQueue) {
			feedingQueue.put(ro);
		}
		
		renderIndexCounter++;
		
		return renderId;		
	}

	private RenderContextObject unpackRCTXValue(RenderContextObject rctxo, double effort) {
		
		if (rctxo.getTransformations().length <= 0) {
			
			//FIXME Unpack Correctly
			if (effort >= 1) {
				throw new UnsupportedOperationException("Unpacking RenderContextObjects with transformations is not supported, yet!");
			} else {
				return rctxo; 
			}
			
		} else {
			return rctxo; 
		}
		
	}
	
	public long enqueueRender(Renderable rnd, RenderContext rctx, ResultSettings rs, long prio, LogInstruction log) {
		return putRenderTask(new RenderObject(rnd, rctx, rs, null, new long[] {prio}, log, log.getOberserver(), new long[0]));
	}
	
	public RenderResult fetchRenderResult(long renderId) {
		while (true) {//TODO Check for errors during rendering and cancel fetch if required
			RenderResult result = finalResultMap.remove(renderId);
			if (result != null) {
				return result;	
			}
		}
	}
	
	public RenderResult runRender(Renderable rnd, RenderContext rctx, ResultSettings rs, long prio, LogInstruction log) {
		long renderId = enqueueRender(rnd, rctx, rs, prio, log);
		return fetchRenderResult(renderId);
	}
	
	public class RenderObject implements ExecutionInterface, Comparable<RenderObject>, Runnable {
		//Data of the RenderObject
		private final Renderable rnd;
		private final RenderContext rctx;
		private final ResultSettings rs;
		private final boolean root;
		private final RenderObject[] renderStack;
		private final long[] priorityStack; //Less = Higher Prio
		private final LogInstruction log; //The observer for the current procedure
		private final LogObserver masterObserver; //Master Observer
		private final long[] masterPath; // Relative path to get to the master observer
		private final AutoCloseable[] toBeClosed; // Objects to be closed after execution
		
		//Fetched data of the RenderObject
		/**Operations/Segments of the RenderObject*/
		private String[] ops;
		
		// Data During Render
		/**Indexing of child renders (Increments on every sub-render)*/
		private long childRenderIndex = 0L;
		private long renderId = -1L; // -1: No current renderId
		private boolean running = false;
		private boolean paused = false;
		private long holdingStatus = -4L; //-4: Holding for nothing
		private volatile RenderResult holdingBuffer = null;
		private RenderResult renderResult = null;
		
		public RenderObject(Renderable rnd, RenderContext rctx, ResultSettings rs, RenderObject[] renderStack, long[] proirityStack, LogInstruction log, LogObserver masterObserver, long[] masterPath, AutoCloseable...toBeClosed) {
			this.renderStack = renderStack;
			this.rnd = rnd;
			this.rctx = rctx;
			this.rs = rs;
			this.root = renderStack == null || renderStack.length < 0;
			this.priorityStack = proirityStack;
			this.ops = RenderUtils.elemOpsFromResultSegmentSettings(rs.getSegments());
			this.log = log;
			this.masterObserver = masterObserver;
			this.masterPath = masterPath;
			this.toBeClosed = toBeClosed;
		}
		
		@Override
		public void run() {
			
			log.log(new LogMessage(LogLevel.Debug, "Run render of: " + rnd));
			
			Stack<Pair<Element, Long>> readySessions = ReadyUtils.makeReadyMinimum(rnd, rctx, ops, 0, null, this);
			
			SubLogObserver rndObs = new SubLogObserver(log.getOberserver(), "EXEC", 0, masterObserver, masterPath);
			
			LogInstruction rndLog = new LogInstruction(rndObs, log);
			
			renderResult = rnd.render(new RenderWrangler(rctx, rs, null, this, null, rndLog)); //FIXME add system-resource interface and ElementAlgorithms
			
			rndObs.close();
			
			for (Pair<Element, Long> readySession : readySessions) {
				//System.out.println("-> UNREADY: " + readySession.getVaue() + "#" + readySession.getExtra());
				readySession.getValue().unready(readySession.getExtra());
			}
			
			for (AutoCloseable toClose : toBeClosed) {
				try {
					toClose.close();
				} catch (Exception e) {
					LogMessage errorMsg = new LogMessage(LogLevel.Error, "Error while closing object \"" + toClose.toString() + "\" - " + e.getClass().getName() + ": " + e.getMessage());
					
					Long[] masterPath = new Long[this.masterPath.length];
					for (int i = 0; i < this.masterPath.length; i++) {
						masterPath[i] = this.masterPath[i];
					}
					errorMsg.appendPath(Arrays.asList(masterPath));
					
					masterObserver.log(errorMsg);
				}
			}
			
			running = false;
		}
		
		public void start() {
			Thread runThread = new Thread(this, "render-" + renderId);
			running = true;
			runThread.start();
		}
		
		public RenderResult getRenderResult() {
			return renderResult;
		}
		
		@Override
		public String toString() {
			StringBuilder priorityStackVis = new StringBuilder();
			if (priorityStack != null && priorityStack.length > 0) {
				for (int i = 0; i < priorityStack.length; i++) {
					priorityStackVis.append("/" + priorityStack[i]);
				}
			} else {
				priorityStackVis.append("none");
			}
			return "RenderObject[" + (running ? ((paused ? "paused - " : "") + "H" + holdingStatus) : (renderResult == null ? "N" : "F")) + "]{RID: " + renderId + ", " + rnd.toString() + "} ~ P:" + priorityStackVis;
		}
		
		public boolean isRoot() {
			return root;
		}

		public void setRenderId(long renderId) {
			this.renderId = renderId;
		}
		
		public long getRenderId() {
			return renderId;
		}
		
		public RenderObject[] getRenderStack() {
			return renderStack;
		}

		public Renderable getRenderable() {
			return rnd;
		}
		
		public RenderContext getRenderContext() {
			return rctx;
		}
		
		public ResultSettings getResultSettings() {
			return rs;
		}
		
		public boolean isRunning() {
			return running;
		}
		
		public void setPaused(boolean paused) {
			this.paused = paused;
		}
		
		public boolean isPaused() {
			return paused;
		}
		
		public long getHoldingStatus() {
			return holdingStatus;
		}

		public synchronized int insertHolding(RenderResult rr, long holdingId) {
			if (holdingStatus == holdingId) {
				holdingBuffer = rr;
				holdingStatus = -2;
				return 0; 	//OK, inserted into holding buffer
			} else {
				return holdingStatus == -2 ? -2 : -3; 	//-3: Wrong id that was holding /  -2: The holding buffer got already filled.
			}
		}

		
		/**
		 * ExecutionInterface
		 */
		
		@Override
		public RenderContextObject unpackRenderContextObject(RenderContextObject rctxo, double effort) {
			return unpackRCTXValue(rctxo, effort);
		}

		@Override
		public long buildRender(Renderable rnd, String[] segments) {
			throw new UnsupportedOperationException("Stepped Render-Build not implemented at the moment");
		}

		@Override
		public RenderableData getRenderableData(long renderId, String[] keys, RenderContext rctx) {
			throw new UnsupportedOperationException("Stepped Render-Build not implemented at the moment");
		}

		@Override
		public String[] getModificationPromises(long renderId, String[] modificationIdents, RenderContext rctx) {
			throw new UnsupportedOperationException("Stepped Render-Build not implemented at the moment");
		}

		@Override
		public int enqueueRender(long renderId, RenderContext rctx, ResultSettings rs, long prio) {
			throw new UnsupportedOperationException("Stepped Render-Build not implemented at the moment");
		}

		@Override
		public long enqueueRender(Renderable rend, RenderContext rctx, ResultSettings rs, long prio) {
			
			SubLogObserver subObserver = new SubLogObserver(log.getOberserver(), ("SUB" + (childRenderIndex+1)), childRenderIndex+1, masterObserver, masterPath);
			
			LogInstruction subLogInstructions = new LogInstruction(subObserver, log);
			
			RenderObject ro = new RenderObject(rend, rctx, rs, 
					appendRenderStack(renderStack, this), 
					appendPriorityStack(priorityStack, prio, childRenderIndex), 
					subLogInstructions, masterObserver, SubLogObserver.appendPath(masterPath, childRenderIndex+1),
					// TO CLOSE:
					subObserver
					);
			
			childRenderIndex++;
			
			long renderId = putRenderTask(ro);
			
			while (paused) {
				try {
					Thread.sleep(10L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			return renderId;
		}

		@Override
		public RenderResult fetchRenderResult(long renderId) {

			holdingStatus = renderId; //Set holding-flag
			
			while (running) {
				
				while (paused) {
					try {
						Thread.sleep(10L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				if (holdingStatus == -2) { //Waiting for flag to be set
					RenderResult bufferCopy = holdingBuffer;
					
					holdingBuffer = null; //Reset buffer
					holdingStatus = -4L; //Remove holding-flag
					
					return bufferCopy; //Found result
				} else {
					try {
						Thread.sleep(1L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					continue;
				}
				
			}

			holdingBuffer = null; //Reset buffer
			holdingStatus = -4L; //Remove holding-flag
			
			return null; //Canceled

		}
		
		@Override
		public long enqueueTarget(Target trgt, long prio) {
			throw new UnsupportedOperationException("Target Execution not implemented at the moment");
		}

		@Override
		public TargetResult fetchTargetResult(long targetId) {
			throw new UnsupportedOperationException("Target Execution not implemented at the moment");
		}
		
		protected long[] getProirityStack() {
			return priorityStack;
		}
		
		/*
		 * 		Comparable
		 */

		@Override
		public int compareTo(RenderObject comp) {
			long[] compStack = comp.getProirityStack();
			int len;
			int longest; //1 = Self Longest; 0 = Same Length; -1 = Other Longest
			if (compStack.length > priorityStack.length) {
				len = priorityStack.length;
				longest = -1;
			} else if (compStack.length == priorityStack.length) {
				len = priorityStack.length;
				longest = 0;
			} else {
				len = compStack.length;
				longest = 1;
			}
			
			//Compare Array Content
			
			for (int i = 0; i < len; i++) {
				if (priorityStack[i] == compStack[i]) {
					continue; //Same: Continue Check
				} else if (priorityStack[i] > compStack[i]) {
					return 1; //Less
				} else {
					return -1; //Higher
				}
			}
			
			//Compared array ranges were same: Check for length
			
			switch (longest) {
			case -1:
				return -1; //Self is Shorter: Higher

			case 0:
				return 0; //Self equal: Equal
				
			default:
				return 1; //Self longer (or everything else): Less
				
			}
			
		}
		
	}

	private static RenderObject[] appendRenderStack(RenderObject[] cStack, RenderObject nObj) {
		int s;
		if (cStack != null) {
			s = cStack.length;
		} else {
			s = 0;
		}
		
		if (s <= 0) {
			return new RenderObject[] {nObj};
		}
		
		RenderObject[] nStack = new RenderObject[s+1];
		
		System.arraycopy(cStack, 0, nStack, 0, s);
		
		nStack[s] = nObj;
		
		return nStack;
		
	}
	
	private static long[] appendPriorityStack(long[] cStack, long customPrio, long childRenderIndex) {
		int s = cStack.length;
		
		long[] nStack = new long[s+2];
		
		System.arraycopy(cStack, 0, nStack, 0, s);

		nStack[s] = customPrio;
		nStack[s+1] = childRenderIndex;
		
		return nStack;
		
	}

}
