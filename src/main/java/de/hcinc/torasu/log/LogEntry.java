package de.hcinc.torasu.log;

public abstract class LogEntry extends LogObject {
	
	public abstract String getMessage();
	
	public abstract LogLevel getLevel();

	public abstract LogEntry clone();
	
	@Override
	public String toString() {
		return "LogEntry[" + getLevel().toString() + "]{\"" + getMessage() + "\"}";
	}
	
	public void copyDataFrom(LogEntry entry) {
		super.copyDataFrom(entry);
	}
	
	/**<h1>Log Level</h1>
	 * The log level describes the importance of a message
	 */
	public static enum LogLevel {
		/**Severe [errors] are errors that are supposed to be system violations and are usually handled with a root-task-shutdown
		 */
		Severe(5),
		
		/**For errors that can not be fixed automatically, but doesn't stop the operation of root-task (e.g.: Rendering a resource that is unable to be loaded: skips render of the current frame)
		 */
		Error(4),
		
		/**Warnings about e.g. discouraged use
		 */
		Warning(3),
		
		/**Progress/Status about computation
		 */
		Info(2),
		
		/**Information e.g.: Optimizations you can do for an ideal situation 
		 */
		Debug(1),
		
		/**Deep Debug information with stuff that goes from general preparation information to detailed computation information (Deepest log level)
		 */
		DebugDeep(0),
		
		/**Debug information with general execution information (e.g.: execution parameters) <br>
		 * For debug information that goes into detail please use {@link LogLevel#DebugDeep}
		 */
		Progress(-1),
		
		/**Insight into performance and speed / useful for Benchmarking the performance of the software
		 */
		Performace(-2),
		
		/**Internal Message of the LOG-System
		 */
		Internal(10), 
		
		/**Placeholder for no logs
		 */
		NONE(100);
		
		private int level;

		LogLevel(int level) {
			this.level = level;
		}
		
		public boolean isContained(LogLevel logLevel) {
			return level <= logLevel.level;
		}
		
	}
	
	/**A normal message with a {@link LogLevel} in {@link LogEntry}-format
	 */
	public static class LogMessage extends LogEntry {
		
		private LogLevel level;
		private String message;

		public LogMessage(LogLevel level, String message) {
			this.level = level;
			this.message = message;
		}
		
		//For cloning
		private LogMessage() {}

		@Override
		public LogLevel getLevel() {
			return level;
		}
		
		@Override
		public String getMessage() {
			return message;
		}

		public void copyDataFrom(LogMessage entry) {
			super.copyDataFrom(entry);
			this.level = entry.level;
			this.message = entry.message;
		}
		
		@Override
		public LogMessage clone() {
			LogMessage clone = new LogMessage();
			clone.copyDataFrom(this);
			return clone;
		}
		
	}
	
	/**A {@link LogEntry} to register a sub-process
	 */
	public static class LogRegister extends LogEntry {

		private long id;
		private String name;
		
		/**Create a {@link LogEntry} to register a sub-process
		 * @param id The ID of the registered sub-process
		 * @param name The name of the registered sub-process
		 */
		public LogRegister(long id, String name) {
			this.id = id;
			this.name = name;
		}
		
		/**@return The ID of the registered sub-process
		 */
		public long getId() {
			return id;
		}
		
		/**@return The name of the registered sub-process
		 */
		public String getName() {
			return name;
		}
		
		//For Cloning
		private LogRegister() {}
		
		@Override
		public String getMessage() {
			return "Register #" + id + " to \"" + name + "\"";
		}

		@Override
		public LogLevel getLevel() {
			return LogLevel.Internal;
		}
		
		public void copyDataFrom(LogRegister entry) {
			super.copyDataFrom(entry);
			this.id = entry.id;
			this.name = entry.name;
		}
		
		@Override
		public LogRegister clone() {
			LogRegister clone = new LogRegister();
			clone.copyDataFrom(this);
			return clone;
		}
		
	}

	/**A {@link LogEntry} to register a sub-process
	 */
	public static class LogUnregister extends LogEntry {

		private long id;
		/**Create a {@link LogEntry} to un-register a sub-process
		 * @param id The ID of the registered sub-process
		 */
		public LogUnregister(long id) {
			this.id = id;
		}
		
		//For cloning
		private LogUnregister() {}
		
		public long getId() {
			return id;
		}
		
		@Override
		public String getMessage() {
			return "Unregister #" + id;
		}

		@Override
		public LogLevel getLevel() {
			return LogLevel.Internal;
		}
		
		public void copyDataFrom(LogUnregister entry) {
			super.copyDataFrom(entry);
			this.id = entry.id;
		}

		@Override
		public LogUnregister clone() {
			LogUnregister clone = new LogUnregister();
			clone.copyDataFrom(this);
			return clone;
		}
		
	}
	
}