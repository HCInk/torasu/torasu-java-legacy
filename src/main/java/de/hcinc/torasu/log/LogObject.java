package de.hcinc.torasu.log;

import java.util.LinkedList;
import java.util.List;

public abstract class LogObject {
	
	private List<Long> path = null;
	
	public List<Long> getPath() {
		return path;
	}

	public void copyDataFrom(LogObject entry) {
		entry.path = new LinkedList<Long>();
		for (Long val : path) {
			entry.path.add(val);
		}
	}
	
	public void appendPath(long id) {
		if (path == null) {
			path = new LinkedList<Long>();
		}
		
		path.add(id);
	}
	
	public void appendPath(List<Long> stack) {
		if (path == null) {
			path = new LinkedList<Long>();
		}
		
		path.addAll(stack);
	}

	public abstract LogObject clone();

}
