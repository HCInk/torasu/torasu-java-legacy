package de.hcinc.torasu.log;

import java.util.Arrays;

import de.hcinc.torasu.log.LogEntry.LogRegister;
import de.hcinc.torasu.log.LogEntry.LogUnregister;

public class SubLogObserver implements LogObserver, AutoCloseable {

	private final LogObserver parent;
	private final String name;
	private final LogObserver directObserver;
	private final long[] directPath;
	
	private int state = 0; //0 = Ready, 1 = Open, 2 = Closed
	private long ownId;
	private long subIdCounter = -1L;
	private Long[] fullPath;
	
	/**Creates sub-observation (Automatic ID-choosing)
	 * @param parent The parent log observer to create a sub-observation in
	 * @param name The name of the sub-observation
	 */
	public SubLogObserver(LogObserver parent, String name) {
		this.parent = parent;
		this.name = name;
		this.ownId = -1L;
		this.directObserver = null;
		this.directPath = null;
	}
	
	/**Creates sub-observation with Fixed ID
	 * @param parent The parent log observer to create a sub-observation in
	 * @param name The name of the sub-observation
	 * @param ownId The id of the sub-observation (NOTE: The set ID has to be unique to the parent - mixing AUTO and FIXED id-systems for the same parent is not supported)
	 */
	public SubLogObserver(LogObserver parent, String name, long ownId) {
		this.parent = parent;
		this.name = name;
		this.ownId = ownId;
		this.directObserver = null;
		this.directPath = null;
	}
	
	/**Creates sub-observation with Fixed ID and a direct logging-route
	 * @param parent The parent log observer to create a sub-observation in
	 * @param name The name of the sub-observation
	 * @param ownId The id of the sub-observation (NOTE: The set ID has to be unique to the parent - mixing AUTO and FIXED id-systems for the same parent is not supported)
	 * @param directObserver The Observer to send the logging-information directly to
	 * @param directPath The ids of the Observers that have been skipped to the given direct-observer (ordered from outer observers to inner observers/root)
	 */
	public SubLogObserver(LogObserver parent, String name, long ownId, LogObserver directObserver, long[] directPath) {
		this.parent = parent;
		this.name = name;
		this.ownId = ownId;
		this.directObserver = directObserver;
		this.directPath = directPath;
	}
	
	@Override
	public void log(LogObject log) {
		boolean ok = false;
		if (state == 1) {
			ok = true;
		} else {
			if (state == 0) {
				
				synchronized (this) {
					if (state == 0) {
						openInternal();
					}
				}
				
				ok = true;
			} else if (state == 2) {
				throw new IllegalAccessError("The Observation was already closed");
			}
		}
		
		if (ok) {
			
			if (directObserver == null) {
				log.appendPath(ownId);
				parent.log(log);
			} else {
				
				synchronized (this) {
					if (fullPath == null) {
						fullPath = new Long[directPath.length+1];
						fullPath[0] = ownId;
						for (int i = 0; i < directPath.length; i++) {
							fullPath[i+1] = directPath[i];
						}
					}	
				}
				
				log.appendPath(Arrays.asList(fullPath));
				directObserver.log(log);
				
			}
			
		} else {
			throw new IllegalStateException("Observation-status is unknown");
		}

	}

	public synchronized void open() {
		if (state == 0) {
			openInternal();
		} else {
			throw new IllegalAccessError("Has already been opened once");
		}
	}
	
	private void openInternal() {
		if (ownId < 0) {
			ownId = parent.newSubId();
		}
		parent.log(new LogRegister(ownId, name));
		state = 1;
	}
	
	@Override
	public synchronized void close() {
		if (state == 1) {
			parent.log(new LogUnregister(ownId));
			state = 2;
		}
	}
	
	//TODO Javadoc
	//Warning doesn't close the output as the normal LogSubObserver#close() would, it just sends an Unregister signal
	public static void closeRemote(LogObserver parentObserver, long id) {
		parentObserver.log(new LogUnregister(id));
	}

	@Override
	public synchronized long newSubId() {
		subIdCounter++;
		return subIdCounter;
	}
	
	//TODO Javadoc
	public static long[] appendPath(long[] path, long toAppend) {
		long[] newPath = new long[path.length+1];
		
		newPath[0] = toAppend;
		System.arraycopy(path, 0, newPath, 1, path.length);
		
		return newPath;
	}
	
}
