package de.hcinc.torasu.log;

import de.hcinc.torasu.log.LogEntry.LogLevel;

//TODO Javadoc
public class LogInstruction {

	public static final LogInstruction NONE = new LogInstruction(null, LogLevel.NONE, false, false);
	
	private final LogObserver lobs;
	private final LogLevel verbosity;
	private final boolean doProgress;
	private final boolean doPerformance;
	
	public LogInstruction(LogObserver lobs, LogLevel verbosity, boolean doProgress, boolean doPerformance) {
		this.lobs = lobs;
		this.verbosity = verbosity;
		this.doProgress = doProgress;
		this.doPerformance = doPerformance;
	}
	
	public LogInstruction(LogObserver lobs, LogInstruction settings) {
		this.lobs = lobs;
		this.verbosity = settings.verbosity;
		this.doProgress = settings.doProgress;
		this.doPerformance = settings.doPerformance;
	}
	
	public LogObserver getOberserver() {
		return lobs;
	}
	
	public LogLevel getVerbosity() {
		return verbosity;
	}
	
	public boolean doProgress() {
		return doProgress;
	}
	
	public boolean doPerformance() {
		return doPerformance;
	}
	
	public boolean log(LogEntry entry) {
		if (lobs != null) {
			boolean doLog;
			LogLevel entryLevel = entry.getLevel();
			
			switch (entryLevel) {
			case Progress:
				doLog = doProgress;
				break;
			case Performace:
				doLog = doPerformance;
				break;
			default:
				doLog = this.verbosity.isContained(entryLevel);
				break;
			}
			
			if (doLog) {
				lobs.log(entry);
				return true;
			}
		}
		return false;
	}
	
}
