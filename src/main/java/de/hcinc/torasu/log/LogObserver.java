package de.hcinc.torasu.log;

public interface LogObserver {

	public void log(LogObject log);
	
	public long newSubId();	
	
}
