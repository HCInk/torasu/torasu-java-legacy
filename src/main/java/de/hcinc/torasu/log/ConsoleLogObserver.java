package de.hcinc.torasu.log;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import de.hcinc.torasu.log.LogEntry.LogRegister;
import de.hcinc.torasu.log.LogEntry.LogUnregister;
import de.hcinc.torasu.util.DataUtils;

public class ConsoleLogObserver implements LogObserver {
	
	public static final int LOG_ONLY = 0;
	public static final int LOG_ERROR_INFO = 1;
	public static final int LOG_EVERYTHING = 2;
	
	private final PrintStream pout;
	private final int logAmmount;
	
	private long subIdCounter = -1L;
	private HashMap<List<Long>, String[]> nameMap = new HashMap<List<Long>, String[]>();

	public ConsoleLogObserver(PrintStream pout) {
		this.pout = pout;
		this.logAmmount = LOG_ONLY;
	}
	
	public ConsoleLogObserver(PrintStream pout, int logAmmount) {
		this.pout = pout;
		this.logAmmount = logAmmount;
	}

	@Override
	public synchronized void log(LogObject log) {
		List<Long> path = log.getPath();
		
		String sPath = "/";
		
		String[] pathStrings = getPathStrings(path);
		
		for (int i = 0; i < pathStrings.length; i++) {
			sPath += pathStrings[i] + "/";
		}
		
		if (log instanceof LogEntry) {

			
			LogEntry entry = (LogEntry) log;
			
			boolean display = true;
			
			if (entry instanceof LogRegister) {
				
				LogRegister logRegister = (LogRegister) entry;
				
				long regId = logRegister.getId();
				String regName = logRegister.getName();
				
				int pathSize = path != null ? path.size() : 0;
				
				if (pathSize > 0) {
					
					// Make Key
					List<Long> pathKey = new LinkedList<Long>(); // Initialize list for key
					pathKey.add(regId);	// Add own id to key
					pathKey.addAll(path); // Copy the parent-path to key
					
					// Make Value
					String[] pathValue = new String[pathSize+1]; // Initialize array for value
					System.arraycopy(pathStrings, 0, pathValue, 0, pathSize); // Copy the parent-path to value
					pathValue[pathSize] = regName; // Add own name to value
					
					// Save value
					nameMap.put(pathKey, pathValue);
					
				} else {
					List<Long> pathKey = new LinkedList<Long>(); // Create new list for key
					pathKey.add(regId); // Add own id to key
					nameMap.put(pathKey, new String[]{regName}); // Create and save value to before created key
				}
				
				display = logAmmount >= LOG_EVERYTHING;
				
			} else if (entry instanceof LogUnregister) {
				LogUnregister logUnregister = (LogUnregister) entry;
				long regId = logUnregister.getId();
				
				int pathSize = path != null ? path.size() : 0;
				List<Long> pathKey = new LinkedList<Long>(); // Initialize list for key
				pathKey.add(regId);	// Add own id to key
				
				if (pathSize > 0) {
					pathKey.addAll(path); // Copy the parent-path to key
				}
				
				boolean removed = nameMap.remove(pathKey) != null;
				
				if (!removed && logAmmount >= LOG_ERROR_INFO) {
					System.out.println("~> LOGGER-ISSUE: Unregister without matching registration recieved: " + DataUtils.formatStringArray(pathKey.toArray()));
				}
				
				display = logAmmount >= LOG_EVERYTHING;
			}
			
			if (display) {
				pout.println("LOG::" + entry.getLevel()  + "\t" + sPath + " \t" + entry.getMessage());
			}
		} else {
			pout.println("LOG::UNK@" + sPath + ": \t" + log.toString());
		}
		
	}

	@Override
	public long newSubId() {
		subIdCounter++;
		return subIdCounter;
	}
	
	private String[] getPathStrings(List<Long> path) {
		
		if (path != null) {
			
			String[] pathStrings = nameMap.get(path);
			
			if (pathStrings == null) {
				int pathSize = path.size();
				
				// Copy the path
				List<Long> compList = new LinkedList<Long>();
				for (int i = 0; i < pathSize; i++) {
					compList.add(path.get(i));
				}
				
				String[] foundPathStrings = null;
				
				// Find a path, by making the path smaller
				while (foundPathStrings == null && !compList.isEmpty()) {
					compList.remove(0);
					foundPathStrings = nameMap.get(compList);
				}
				
				if (foundPathStrings == null) {
					foundPathStrings = new String[0];
				}
				
				int foundPathSize = foundPathStrings.length;
				
				pathStrings = new String[pathSize];

				// Write found part
				for (int i = 0; i < foundPathSize; i++) {
					pathStrings[i] = foundPathStrings[i];
				}
				
				// Write unknown part
				for (int i = foundPathSize; i < pathSize; i++) {
					pathStrings[i] = "~" + path.get(pathSize-i-1);
					
				}
				
				nameMap.put(path, pathStrings);
			}
			
			return pathStrings;
		} else {
			return new String[0];
		}
	}
	
}

